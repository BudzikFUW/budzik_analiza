#!/usr/bin/env python2

import os.path
import sys
from xml.etree import ElementTree

import mne
import numpy
import pylab
import scipy.signal


def read_events(path_to_tag, sfreq, tmin):
    events = []
    xml_tags = ElementTree.parse(path_to_tag).getroot()
    for tag in xml_tags.iter('tag'):
        name = tag.attrib['name'].lower()
        if name == 'erds_instr1.wav':
            event_id = 1
        elif name == 'erds_instr2.wav':
            event_id = 2
        else:
            continue
        position = float(tag.attrib['position'])
        if position + tmin > 0:
            offset = int(sfreq * position)
            events.append([offset, 0, event_id])
    return numpy.array(events, dtype='int')


def read_raw_array(path_to_raw, path_to_xml):
    RS = '{http://signalml.org/rawsignal}'
    xml_spec = ElementTree.parse(path_to_xml).getroot()
    sfreq = int(float(xml_spec.find(RS + 'samplingFrequency').text))
    channel_count = int(xml_spec.find(RS + 'channelCount').text)
    sample_count = int(xml_spec.find(RS + 'sampleCount').text)

    channel_names = list(map(
        lambda tag: tag.text,
        xml_spec.iter(RS + 'label')
    ))

    gains = numpy.array(list(map(
        lambda tag: float(tag.text),
        xml_spec.find(RS + 'calibrationGain').iter(
            RS + 'calibrationParam')
    )))

    data = numpy.fromfile(path_to_raw, dtype='f4').reshape(
        [sample_count, channel_count]
    ) * (1.0e-6 * gains)

    nfreq = 0.5 * sfreq
    b, a = scipy.signal.iirdesign(
        70.0/nfreq,
        60.0/nfreq,
        1.0, 15.0, ftype='butter'
    )
    # w, h = scipy.signal.freqz(b, a, 1000)
    # pylab.plot(nfreq*w/numpy.pi, 40*numpy.log10(abs(h)))  # 40 bo filtfilt
    # pylab.ylabel('response [dB]')
    # pylab.xlabel('frequency [Hz]')
    # pylab.ylim(-50, 5)
    # pylab.show()
    data = scipy.signal.filtfilt(b, a, data, axis=0)

    info = mne.create_info(channel_names, sfreq)
    array = mne.io.RawArray(data.T, info)
    return array


################################################################################

T_MIN = 0
T_MAX = 8

################################################################################

if len(sys.argv) < 2:
    sys.exit("USAGE: {} path-to-signal.obci.raw ...".format(sys.argv[0]))

try:
    mne.set_log_level('WARNING')
    for path_to_raw in sys.argv[1:]:
        if path_to_raw[-9:] != '.obci.raw':
            raise Exception("invalid path")
        core_of_path = path_to_raw[:-9]
        core_of_name = os.path.basename(core_of_path)

        path_to_xml = core_of_path + '.obci.xml'
        path_to_tag = core_of_path + '.obci.tag'
        raw_array = read_raw_array(path_to_raw, path_to_xml)
        events = read_events(path_to_tag, raw_array.info['sfreq'], T_MIN)
        event_id = {'hand': 1, 'leg': 2}
        epochs = mne.Epochs(raw_array, events, event_id=event_id, tmin=T_MIN, tmax=T_MAX, add_eeg_ref=False,
                           preload=True)

        def get_power_values(off_type, channels):
            power_values = []
            for epoch in epochs[off_type][1:]:
                power = 0
                for channel in channels:
                    power += sum(epoch[channel]**2)
                power_values.append(power)
            return power_values

        def compute_p_value(channels, alternative):
            power_values_on_hand = get_power_values('hand', channels)
            power_values_on_leg = get_power_values('leg', channels)
            U, p = scipy.stats.mannwhitneyu(
                power_values_on_hand,
                power_values_on_leg,
                alternative=alternative
            )
            return p

        hand_channels = []
        if "l_reka" in epochs.ch_names:
            hand_channels.append(epochs.ch_names.index("l_reka"))
        if "p_reka" in epochs.ch_names:
            hand_channels.append(epochs.ch_names.index("p_reka"))
        leg_channels = []
        if "l_noga" in epochs.ch_names:
            leg_channels.append(epochs.ch_names.index("l_noga"))
        if "p_noga" in epochs.ch_names:
            leg_channels.append(epochs.ch_names.index("p_noga"))

        print "%.3f %.3f %s" % (
            compute_p_value(hand_channels, 'greater'),
            compute_p_value(leg_channels, 'less'),
            core_of_name
        )

except Exception as e:
    sys.exit('ERROR: {}'.format(e))
