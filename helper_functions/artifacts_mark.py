#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Parametric Detection of EEG Artifacts
# Marcin Pietrzak 2017-03-01
# based on Klekowicz et al. DOI 10.1007/s12021-009-9045-2

import os
import sys
import json
from copy import deepcopy

import numpy as np
import matplotlib.pyplot as plt

from obci.analysis.obci_signal_processing.tags.tags_file_writer import TagsFileWriter

from helper_functions import mgr_filter, get_microvolt_samples, Power, BandPower
from helper_functions import map1020, channels_not2draw, set_axis_fontsize

from config import figure_scale, fontsize

#  progi odnoszące się do std lub mediany są szacowane dla każdego kanału osobno
#  tagi są zaznaczane wg najostrzejszego z kryteriów
#  żeby nie korzystać z danego kryterium wystarczy podać wartość np.inf
#  każdy tag ma zapisane informacje:
#   - które kryterium było wykorzystane (typ:'abs', 'std' lub 'med')
#   - jaki był ostateczny próg (thr:wartość) w µV
#   - jaka wartość przekroczyła próg (val:wartość) w µV

DEBUG = False
DEBUG_FS = 1024.

DefaultArtRejDict = {
    'SlopeWindow': 0.0704,  # [s] #szerokość okna, w którym szukamy iglic i stromych zboczy (slope)
    'SlowWindow': 0.5,  # [s] #szerokość okna, w którym szukamy fal wolnych (slow)
    
    'OutliersMergeWin': .1,  # [s] #szerokość okna, w którym łączymy sąsiadujące obszary z próbkami outlier
    
    'MusclesFreqRange': [40, 90],  # [Hz] przedział częstości, w którym szukamy artefaktów mięśniowych
    
    'SlopesAbsThr': np.inf,  # [µV] bezwzględna wartość amplitudy peak2peak w oknie SlopeWindow
    'SlopesStdThr': 7,  # wartość peak2peak w oknie, jako wielokrotność std
    'SlopesMedThr': 6,  # wartość peak2peak w oknie, jako wielokrotność mediany
    
    'SlowsAbsThr': np.inf,  # [µV] bezwzględna wartość amplitudy peak2peak w oknie SlowWindow
    'SlowsStdThr': 6,  # wartość peak2peak w oknie, jako wielokrotność std
    'SlowsMedThr': 4,  # wartość peak2peak w oknie, jako wielokrotność mediany
    
    'OutliersAbsThr': np.inf,  # [µV] bezwzględna wartość amplitudy (liczona od 0)
    'OutliersStdThr': 7,  # wartość amplitudy, jako wielokrotność std
    'OutliersMedThr': 13,  # wartość amplitudy, jako wielokrotność mediany
    
    'MusclesAbsThr': np.inf,  # [µV²] bezwzględna wartość średniej mocy na próbkę w zakresie częstości MusclesFreqRange
    'MusclesStdThr': 7,  # wartość amplitudy, jako wielokrotność std
    'MusclesMedThr': 5,  # wartość amplitudy, jako wielokrotność mediany
}

PossibleChannels = ['Fp1', 'Fpz', 'Fp2', 'F7', 'F3', 'Fz', 'F4', 'F8', 'T3', 'C3',
                    'Cz', 'C4', 'T4', 'T5', 'P3', 'Pz', 'P4', 'T6', 'O1', 'Oz', 'O2',
                    'M1', 'M2']

artifact_labels = ["arbitrary", "median", "std"]

def getDefaultArtRejDict():
    return DefaultArtRejDict


def _validate_channels(bad_samples, thresholds, priviliged_channels = ()):
    ch_validation = {}
    for ch in thresholds:
        bs_ratio = 1. * np.sum(bad_samples[ch] > 0) / len(bad_samples[ch])
        
        ok = True
        # odrzucamy kanał, jeśli
        if max(thresholds[ch]) > thresholds[ch][0]:  # progi statystyczne przekroczyły próg absolutny (arbitralny)
            if max(thresholds[ch]) > 2 * thresholds[ch][0] or ch not in priviliged_channels:  # dwukrotnie dla kanałów uprzywilejowanych (zwykle Fpx)
                print "THR invalid", ch, thresholds[ch][0]
                ok = False
        
        # lub jeśli
        elif bs_ratio > 0.2:  # liczba złych próbek przekracza 20%
            if bs_ratio > 0.5 or ch not in priviliged_channels:  # 50% dla kanałów uprzywilejowanych
                print "BSR invalid", ch, bs_ratio
                ok = False
        
        # w przypadku, gdy thresholds[ch][0] zawiera wartość np.inf, potrzebne jest inne kryterium:
        elif thresholds[ch][0] == np.inf:
            thrs = [thresholds[c][1] for c in thresholds]
            if thresholds[ch][1] > np.median(thrs) * 5:  # jeśli threshold danego typu dla danego kanału 5-krotnie przekracza medianę
                ok = False  # thresholdów tego typu liczoną po wszystkich kanałach, to wywalamy ten kanał
            thrs = [thresholds[c][2] for c in thresholds]
            if thresholds[ch][2] > np.median(thrs) * 5:
                ok = False
        ch_validation[ch] = ok
    
    return ch_validation


def ValidateChannels_Artifacts(eeg_rm, priviliged_channels = ()):
    # filtrowanie dla zwykłych artefaktów:
    filters = [[1., 0.5, 3, 6.97, "butter"],
               [30, 60, 3, 12.4, "butter"],
               [[47.5, 52.5], [49.9, 50.1], 3, 25, "cheby2"]]
    
    # filtrowanie dla detekcji mięśni:
    line_fs = [50.]
    filters_muscle = []
    for line_f in line_fs:
        filters_muscle.append([[line_f - 2.5, line_f + 2.5], [line_f - 0.1, line_f + 0.1], 3.0, 25., "cheby2"])
    
    available_chnls = eeg_rm.get_param('channels_names')
    
    # kanały, które będą testowane:
    ch_2be_tested = [c for c in PossibleChannels if c in available_chnls]
    
    bad_samples, thresholds = outliers(eeg_rm, ch_2be_tested, filters, StdThr = DefaultArtRejDict['OutliersStdThr'],
                                       AbsThr = DefaultArtRejDict['OutliersAbsThr'], MedThr = DefaultArtRejDict['OutliersMedThr'],
                                       full_output = True)  # zaznaczanie outliers
    
    CH_validation_Out = _validate_channels(bad_samples, thresholds, priviliged_channels)
    
    bad_samples, thresholds = P2P(eeg_rm, ch_2be_tested, DefaultArtRejDict['SlowWindow'], filters, StdThr = DefaultArtRejDict['SlowsStdThr'],
                                  AbsThr = DefaultArtRejDict['SlowsAbsThr'], MedThr = DefaultArtRejDict['SlowsMedThr'], kind = 'slows',
                                  full_output = True)  # zaznaczanie fal wolnych
    
    CH_validation_Slows = _validate_channels(bad_samples, thresholds, priviliged_channels)
    
    bad_samples, thresholds = P2P(eeg_rm, ch_2be_tested, DefaultArtRejDict['SlopeWindow'], filters, StdThr = DefaultArtRejDict['SlopesStdThr'],
                                  AbsThr = DefaultArtRejDict['SlopesAbsThr'], MedThr = DefaultArtRejDict['SlopesMedThr'], kind = 'slopes',
                                  full_output = True)  # zaznaczanie iglic i stromych zboczy
    
    CH_validation_Slopes = _validate_channels(bad_samples, thresholds, priviliged_channels)
    
    bad_samples, thresholds = muscles(eeg_rm, ch_2be_tested, DefaultArtRejDict['SlowWindow'], DefaultArtRejDict['MusclesFreqRange'], filters_muscle,
                                      # zaznaczanie mięśni
                                      StdThr = DefaultArtRejDict['MusclesStdThr'], AbsThr = DefaultArtRejDict['MusclesAbsThr'],
                                      MedThr = DefaultArtRejDict['MusclesMedThr'], full_output = True)
    
    CH_validation_Musc = _validate_channels(bad_samples, thresholds, priviliged_channels)
    
    print "Rezultat walidacji kanałów ze względu na artefakty (kanały odrzucone):"
    print "zbyt wiele outlierów:                ",
    for ch in CH_validation_Out:
        if not CH_validation_Out[ch]:
            print ch, ' ',
    
    print "\nzbyt wiele zakłóceń wolnofalowych: ",
    for ch in CH_validation_Slows:
        if not CH_validation_Slows[ch]:
            print ch, ' ',
    
    print "\nzbyt wiele stromych zboczy        :",
    for ch in CH_validation_Slopes:
        if not CH_validation_Slopes[ch]:
            print ch, ' ',
    
    print "\nzbyt wiele mięśni                 :",
    for ch in CH_validation_Musc:
        if not CH_validation_Musc[ch]:
            print ch, ' ',
    print
    
    ch_validation = {}
    for ch in CH_validation_Out:
        ch_validation[ch] = CH_validation_Out[ch] * CH_validation_Slows[ch] * CH_validation_Slopes[ch] * CH_validation_Musc[ch]
    
    return ch_validation


def ValidateChannels_Noise(eeg_rm):
    available_chnls = eeg_rm.get_param('channels_names')
    
    # kanały, które będą testowane:
    ch_2be_tested = [c for c in PossibleChannels if c in available_chnls]
    
    # Power of Noise
    noise_band = [52, 98]
    noise_thresholds = [2e1, 2e2, 2e3]
    width = 1.
    __, bad_channels = BandPower(eeg_rm, ch_2be_tested, width, noise_band, "-", noise_thresholds, plot = False)
    ch_validation = {ch: ch not in bad_channels for ch in ch_2be_tested}
    
    print "\nZbyt wysoki poziom szumów:",
    for ch in ch_validation:
        if not ch_validation[ch]:
            print ch, ' ',
    print
    
    return ch_validation


def RM_ArtifactDetection(eeg_rm, all_channels, ArtDet_kargs = {}, outpath = ""):
    
    channels = ['Fp1', 'Fpz', 'Fp2', 'F7', 'F3', 'Fz', 'F4', 'F8', 'T3', 'C3',
                'Cz', 'C4', 'T4', 'T5', 'P3', 'Pz', 'P4', 'T6', 'O1', 'Oz', 'O2']
    available_chnls = eeg_rm.get_param('channels_names')
    
    # kanały, które będą testowane:
    ch_2be_tested = [c for c in channels if c in available_chnls]
    
    # filtrowanie dla zwykłych artefaktów:
    filters = [[1., 0.5, 3, 6.97, "butter"],
               [30, 60, 3, 12.4, "butter"],
               [[47.5, 52.5], [49.9, 50.1], 3, 25, "cheby2"]]
    
    # filtrowanie dla detekcji mięśni:
    line_fs = [50.]
    filters_muscle = []
    for line_f in line_fs:
        filters_muscle.append([[line_f - 2.5, line_f + 2.5], [line_f - 0.1, line_f + 0.1], 3.0, 25., "cheby2"])
    
    # jeśli którys z poniższych parametrów nie został podany ręcznie, to jest używana wartość domyślna
    forget = ArtDet_kargs.get('forget', 2.)  # [s] długość fragmentów na początku i końcu sygnału, dla których nie zapisujemy tagów
    
    SlopeWindow = ArtDet_kargs.get('SlopeWindow', DefaultArtRejDict['SlopeWindow'])
    SlowWindow = ArtDet_kargs.get('SlowWindow', DefaultArtRejDict['SlowWindow'])
    OutliersMergeWin = ArtDet_kargs.get('OutliersMergeWin', DefaultArtRejDict['OutliersMergeWin'])
    MusclesFreqRange = ArtDet_kargs.get('MusclesFreqRange', DefaultArtRejDict['MusclesFreqRange'])
    
    # progi powyżej których próbka jest oznaczana, jako slope:
    SlopesAbsThr = ArtDet_kargs.get('SlopesAbsThr', DefaultArtRejDict['SlopesAbsThr'])
    SlopesStdThr = ArtDet_kargs.get('SlopesStdThr', DefaultArtRejDict['SlopesStdThr'])
    SlopesMedThr = ArtDet_kargs.get('SlopesMedThr', DefaultArtRejDict['SlopesMedThr'])
    SlopesThrs = (SlopesAbsThr, SlopesStdThr, SlopesMedThr)
    
    # progi powyżej których próbka jest oznaczana, jako slow (fala wolna):
    SlowsAbsThr = ArtDet_kargs.get('SlowsAbsThr', DefaultArtRejDict['SlowsAbsThr'])
    SlowsStdThr = ArtDet_kargs.get('SlowsStdThr', DefaultArtRejDict['SlowsStdThr'])
    SlowsMedThr = ArtDet_kargs.get('SlowsMedThr', DefaultArtRejDict['SlowsMedThr'])
    SlowsThrs = (SlowsAbsThr, SlowsStdThr, SlowsMedThr)
    
    # progi powyżej których próbka jest oznaczana, jako outlier:
    OutliersAbsThr = ArtDet_kargs.get('OutliersAbsThr', DefaultArtRejDict['OutliersAbsThr'])
    OutliersStdThr = ArtDet_kargs.get('OutliersStdThr', DefaultArtRejDict['OutliersStdThr'])
    OutliersMedThr = ArtDet_kargs.get('OutliersMedThr', DefaultArtRejDict['OutliersMedThr'])
    OutliersThrs = (OutliersAbsThr, OutliersStdThr, OutliersMedThr)
    
    # progi powyżej których próbka jest oznaczana, jako muscle:
    MusclesAbsThr = ArtDet_kargs.get('MusclesAbsThr', DefaultArtRejDict['MusclesAbsThr'])
    MusclesStdThr = ArtDet_kargs.get('MusclesStdThr', DefaultArtRejDict['MusclesStdThr'])
    MusclesMedThr = ArtDet_kargs.get('MusclesMedThr', DefaultArtRejDict['MusclesMedThr'])
    MusclesThrs = (MusclesAbsThr, MusclesStdThr, MusclesMedThr)
    
    return detect_artifacts(eeg_rm, ch_2be_tested, all_channels, filters, filters_muscle, forget, OutliersMergeWin, SlopeWindow, SlowWindow, MusclesFreqRange,
                            SlopesThrs, SlowsThrs, OutliersThrs, MusclesThrs, outpath)


def detect_artifacts(eeg_rm, channels, all_channels, filters, filters_muscle, forget,
                     OutliersMergeWin, SlopeWindow, SlowWindow, MusclesFreqRange, SlopesThrs, SlowsThrs, OutliersThrs, MusclesThrs,
                     outpath = ""):
    
    SlopesAbsThr, SlopesStdThr, SlopesMedThr = SlopesThrs
    SlowsAbsThr, SlowsStdThr, SlowsMedThr = SlowsThrs
    OutliersAbsThr, OutliersStdThr, OutliersMedThr = OutliersThrs
    MusclesAbsThr, MusclesStdThr, MusclesMedThr = MusclesThrs
    
    Fs = float(eeg_rm.get_param('sampling_frequency'))
    
    tags = []
    
    bad_samples, thresholds = outliers(eeg_rm, channels, filters, StdThr = OutliersStdThr, AbsThr = OutliersAbsThr, MedThr = OutliersMedThr,
                                       outpath = outpath)  # zaznaczanie outliers
    bad_samples = ForgetBirthAndDeath(bad_samples, Fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    tags.extend(ArtTagsWriter(bad_samples, Fs, all_channels, MergWin = OutliersMergeWin, ArtName = "outlier", Thresholds = thresholds))
    
    bad_samples, thresholds = P2P(eeg_rm, channels, SlowWindow, filters, StdThr = SlowsStdThr, AbsThr = SlowsAbsThr, MedThr = SlowsMedThr,
                                  kind = 'slows', outpath = outpath)  # zaznaczanie fal wolnych
    bad_samples = ForgetBirthAndDeath(bad_samples, Fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    tags.extend(ArtTagsWriter(bad_samples, Fs, all_channels, MergWin = SlowWindow, ArtName = "slow", Thresholds = thresholds))
    
    bad_samples, thresholds = P2P(eeg_rm, channels, SlopeWindow, filters, StdThr = SlopesStdThr, AbsThr = SlopesAbsThr, MedThr = SlopesMedThr,
                                  kind = 'slopes', outpath = outpath)  # zaznaczanie iglic i stromych zboczy
    bad_samples = ForgetBirthAndDeath(bad_samples, Fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    tags.extend(ArtTagsWriter(bad_samples, Fs, all_channels, MergWin = SlopeWindow, ArtName = "slope", Thresholds = thresholds))
    
    bad_samples, thresholds = muscles(eeg_rm, channels, SlowWindow, MusclesFreqRange, filters_muscle, StdThr = MusclesStdThr, AbsThr = MusclesAbsThr,
                                      MedThr = MusclesMedThr, outpath = outpath)  # zaznaczanie mięśni
    bad_samples = ForgetBirthAndDeath(bad_samples, Fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    tags.extend(ArtTagsWriter(bad_samples, Fs, all_channels, MergWin = SlowWindow, ArtName = "muscle", Thresholds = thresholds))
    
    #    to będzie musiało być w innym miejscu (po akceptacji przez użytkownika)
    #    ArtTagsSaver(artifacts_file_path,tags) #zapis tagów do pliku
    
    return tags


def get_dirty_epochs_ids(epochs_list, artifact_tags, eeg_rm = None,
                         head_min_overlap = 0.015, tail_min_overlap = 0.05, all_channels = None, art_det_kargs = {}, art_hist_outpath = ""):
    """Return ids of epochs with(!) artifacts

    :param epochs_list: list of all epochs to investigate
    :param artifact_tags: list of artifacts provided by user, if empty artifacts will be detected here and list will be filled
    :param eeg_rm: read_manager containing complete signal (but without dropped channels)
    :param head_min_overlap: minimal overlap [s] of epoch and artifact on the begging of an epoch
    :param tail_min_overlap: minimal overlap [s] of epoch and artifact on the end of an epoch
    :param all_channels: list of all channels in original file
    :param art_det_kargs: optional custom arguments for artifact detection
    :return: ids of dirty epochs
    """
    
    if not artifact_tags:
        for tag in RM_ArtifactDetection(eeg_rm, all_channels, art_det_kargs, art_hist_outpath):
            artifact_tags.append(tag)
    # tablica granic wszystkich artefaktów bez względu na kanał:
    ARTS = {'outlier': 0, 'slow': 1, 'slope': 2, 'muscle': 3}
    COLORS = [(0.9, 0., 0., 1.), (0.6, 0., 0.6, 1.), (0.6, 0.6, 0., 1.), (0.1, 0.1, 0.8, 1.)]
    ArtifactsBorders = np.array(
        [[-np.inf, -np.inf, 0]] + [[tag['start_timestamp'], tag['end_timestamp'], ARTS[tag['name']]] for tag in artifact_tags if tag['name'] in ARTS])
    # pętla po odcinkach z bodźcami i zapisywanie numerów tych, w których wystąpił jakiś artefakt:
    dirty_epochs_ids = {}
    epoch_id = -1
    for type_id, epoch_by_type in enumerate(epochs_list):
        for epoch in epoch_by_type:
            epoch_id += 1
            # początek i koniec odcinka, który badamy
            start = float(epoch.get_start_timestamp()) + head_min_overlap
            end = float(epoch.get_end_timestamp()) - tail_min_overlap

            artifacts_starting_before_end = ArtifactsBorders[:, 0] < end
            artifacts_ending_after_start = ArtifactsBorders[:, 1] > start
            # wystarczy każde, choćby niewielkie przekrycie
            
            artifacts_overlapping = artifacts_starting_before_end * artifacts_ending_after_start
            
            if np.any(artifacts_overlapping):
                ID = np.argmax(artifacts_overlapping)
                dirty_epochs_ids[epoch_id] = COLORS[int(ArtifactsBorders[ID, 2])]
    return dirty_epochs_ids


def artifacts_epochs_cleaner(epochs_list, artifact_tags, start_offset, duration):
    # reject epochs with artifacts
    
    # tablica granic wszystkich artefaktów bez względu na kanał:
    ArtifactsBorders = np.array([[tag['start_timestamp'], tag['end_timestamp']] for tag in artifact_tags])
    
    # pętla po odcinkach z bodźcami i zapisywanie tylko tych, w których nie wystąpił żaden artefakt:
    cleaned_epochs = [[] for e in epochs_list]
    for type_id, epoch_by_type in enumerate(epochs_list):
        for epoch in epoch_by_type:
            
            # początek i koniec odcinka, który badamy
            start = float(epoch.get_start_timestamp()) - start_offset
            end = start + duration
            
            artifacts_starting_before_end = ArtifactsBorders[:, 0] < end
            artifacts_ending_after_start = ArtifactsBorders[:, 1] > start
            # wystarczy każde, choćby niewielkie przekrycie
            
            artifacts_overlapping = artifacts_starting_before_end * artifacts_ending_after_start
            
            if not np.any(artifacts_overlapping):
                cleaned_epochs[type_id].append(epoch)
    
    print "Number of epochs types:", len(cleaned_epochs)
    for i in range(len(epochs_list)):
        print "clean epochs: {} of {} stimulus".format(len(cleaned_epochs[i]), len(epochs_list[i]))
    # raw_input("Press Enter to continue")
    
    return cleaned_epochs


def ForgetBirthAndDeath(bs, Fs, forget = 1.):
    for ch in bs:
        bs[ch][:int(Fs * forget)] = 0.0
        bs[ch][-int(Fs * forget):] = 0.0
    return bs


def outliers(rm, channels, filters = [], StdThr = np.inf, AbsThr = np.inf, MedThr = np.inf, full_output = False, outpath = ""):
    # w gruncie rzeczy to jeszcze można zrobić test, czy rozkład jest gaussowski,
    # bo czasami nie jest i wtedy można sugerować kanał, jako wadliwy
    # może to m.in. sugerować, że właściwości statystyczne kanału nie są stałe w czasie, np. odkleił się
    if outpath:
        fig, axs = plt.subplots(6, 5, figsize = (1. * 5 * figure_scale, .5625 * 5 * figure_scale))
        for ch in channels_not2draw(channels):
            pos = map1020[ch]
            axs[pos.y, pos.x].axis("off")
        fig.subplots_adjust(left = 0.03, bottom = 0.03, right = 0.98, top = 0.95, wspace = 0.16, hspace = 0.28)

    rm = deepcopy(rm)
    for filter in filters:
        print "Filtering...", filter
        rm = mgr_filter(rm, filter[0], filter[1], filter[2],
                        filter[3], ftype = filter[4], use_filtfilt = True)
    
    bad_samples = dict()
    ThresholdsDict = dict()
    AllThresholdsDict = dict()
    
    for ch in channels:
        c = get_microvolt_samples(rm, ch)
        c -= c.mean()
        
        # obliczenie progu dla std
        C = c.copy()
        s = np.inf
        while C.std() < s:
            s = C.std()
            C = C[np.abs(C) < StdThr * s + np.median(C)]

        C_abs = np.abs(C)
        Thresholds = (AbsThr, MedThr * np.median(C_abs), StdThr * s + np.median(C_abs))
        
        Threshold = min(Thresholds)  # wybieramy najostrzejszy (najniższy) próg
        ThresholdType = ['abs', 'med', 'std'][np.argmin(Thresholds)]
        
        print "{:>4}(outliers) - abs: {:.3g}, med: {:.3g}, std: {:.3g}, chosen: {}".format(*(ch,) + Thresholds + (ThresholdType,))
        if Threshold <= 0:
            raise Exception("Threshold = {}\nYou must give some threshold in range (0,+oo)!".format(Threshold))
        
        ThresholdsDict[ch] = (Threshold, ThresholdType)
        AllThresholdsDict[ch] = Thresholds
        c_abs = np.abs(c)
        bad_samples[ch] = (c_abs > Threshold) * c_abs

        if outpath:
            bins = max(20, c_abs.size // 1000)
            pos = map1020[ch]
            ax = axs[pos.y, pos.x]
            c_abs[c_abs > 3*Threshold] = 3*Threshold  # trzeba przyciąć takie przypadki, bo potrafią rozwalić cały histogram

            ax.hist(c_abs, bins = bins)
            mx = ax.get_ylim()[1]
            for i in range(3):
                if Thresholds[i] < np.inf:
                    ax.plot([Thresholds[i], Thresholds[i]], [0, mx], label = artifact_labels[i])
            ax.plot([Threshold], [mx], "o", label = ThresholdType)
            ax.set_title("{} {}".format("outliers", ch))
            set_axis_fontsize(ax, fontsize * figure_scale / 8)
            ax.legend(fontsize = fontsize * figure_scale / 8)


    if outpath:
        print "saving figures...", outpath
        fig.savefig(outpath.strip(".obci.tag")+"_outliers.png")
        plt.close(fig)


    if full_output:
        return bad_samples, AllThresholdsDict
    else:
        return bad_samples, ThresholdsDict


def P2P(rm, channels, width, filters = [], StdThr = np.inf, AbsThr = np.inf, MedThr = np.inf, kind = 'P2P', full_output = False, outpath = ""):
    if outpath:
        fig, axs = plt.subplots(6, 5, figsize = (1. * 5 * figure_scale, .5625 * 5 * figure_scale))
        for ch in channels_not2draw(channels):
            pos = map1020[ch]
            axs[pos.y, pos.x].axis("off")
        fig.subplots_adjust(left = 0.03, bottom = 0.03, right = 0.98, top = 0.95, wspace = 0.16, hspace = 0.28)
    
    rm = deepcopy(rm)
    for filter in filters:
        print "Filtering...", filter
        rm = mgr_filter(rm, filter[0], filter[1], filter[2],
                        filter[3], ftype = filter[4], use_filtfilt = True)
    
    Fs = int(float(rm.get_param('sampling_frequency')))
    width = int(width * Fs)
    bad_samples = dict()
    ThresholdsDict = dict()
    AllThresholdsDict = dict()
    for ch in channels:
        c = get_microvolt_samples(rm, ch)
        
        mm = MiniMax(c, width, ch)
        # przesuniecie o polowe szerokosci okna i ponowne obliczenie
        mm_2 = MiniMax(np.roll(c, -width / 2), width, ch)
        
        # obliczenie progu dla std
        MM = np.concatenate([mm, mm_2])
        if outpath:
            mm_for_hist = MM.copy()
        s = np.inf
        while MM.std() < s:
            s = MM.std()
            MM = MM[MM < StdThr * s + np.median(MM)]
        
        Thresholds = (AbsThr, MedThr * np.median(MM), StdThr * s + np.median(MM))
        Threshold = min(Thresholds)  # wybieramy najostrzejszy (najnizszy) prog
        ThresholdType = ['abs', 'med', 'std'][np.argmin(Thresholds)]
        print "{:>4}({}) - abs: {:.3g}, med: {:.3g}, std: {:.3g}, chosen: {}".format(*(ch, kind) + Thresholds + (ThresholdType,))
        
        if Threshold <= 0:
            raise Exception("Threshold = {}\nYou must give some threshold in range (0,+oo)!".format(Threshold))
        bad_segment = mm * (mm > Threshold)  # jednostronnie, bo interesują nas tylko przypadki o zbyt dużej, a nie zbyt małej amplitudzie
        bad_segment_2 = mm_2 * (mm_2 > Threshold)  # jednostronnie przesunięte
        
        ThresholdsDict[ch] = (Threshold, ThresholdType)
        AllThresholdsDict[ch] = Thresholds
        bad_samples[ch] = np.max([BadSeg2BadSamp(bad_segment, width, c.size), BadSeg2BadSamp(bad_segment_2, width, c.size, width / 2)], axis = 0)
        
        if DEBUG and kind == "slows" and ch == "Fp1":
            timeline = np.arange(c.size, dtype = float)/DEBUG_FS
            right = 0
            while np.argmax(bad_samples[ch][right:]) > 0:
                left = right + np.argmax(bad_samples[ch][right:].astype(bool))
                right = left + bad_samples[ch][left:].argmin()
                print "{} {}".format(ch, timeline[left])
                plt.figure()
                plt.title("{} {}".format(ch, timeline[left]))
                plt.plot(timeline[left-10:right+10], c[left-10:right+10])
                plt.plot(timeline[left-10:right+10], bad_samples[ch][left-10:right+10])
                # plt.show()

        
        if outpath:
            bins = max(20, mm_for_hist.size//100)
            pos = map1020[ch]
            ax = axs[pos.y, pos.x]
            mm_for_hist[mm_for_hist > 3*Threshold] = 3*Threshold  # trzeba przyciąć takie przypadki, bo potrafią rozwalić cały histogram
            ax.hist(mm_for_hist, bins = bins)
            mx = ax.get_ylim()[1]
            for i in range(3):
                if Thresholds[i] < np.inf:
                    ax.plot([Thresholds[i], Thresholds[i]], [0, mx], label = artifact_labels[i])
            ax.plot([Threshold], [mx], "o", label = ThresholdType)
            ax.set_title("{} {}".format(kind, ch))
            set_axis_fontsize(ax, fontsize * figure_scale / 8)
            ax.legend(fontsize = fontsize * figure_scale / 8)
 
    if outpath:
        fig.savefig(outpath.strip(".obci.tag")+"_{}.png".format(kind))
        plt.close(fig)
 
    # plt.plot(c[bad_samples[ch] > 0])
    # plt.plot(bad_samples[ch][bad_samples[ch] > 0], 'r')
    # plt.title(ch)
    # plt.show()

    if full_output:
        return bad_samples, AllThresholdsDict
    else:
        return bad_samples, ThresholdsDict


def muscles(rm, channels, width, rng, filters = [], StdThr = np.inf, AbsThr = np.inf, MedThr = np.inf, full_output = False, outpath = ""):
    if outpath:
        fig, axs = plt.subplots(6, 5, figsize = (1. * 5 * figure_scale, .5625 * 5 * figure_scale))
        for ch in channels_not2draw(channels):
            pos = map1020[ch]
            axs[pos.y, pos.x].axis("off")
        fig.subplots_adjust(left = 0.03, bottom = 0.03, right = 0.98, top = 0.95, wspace = 0.16, hspace = 0.28)
    
    rm = deepcopy(rm)
    for filter in filters:
        print "Filtering...", filter
        rm = mgr_filter(rm, filter[0], filter[1], filter[2],
                        filter[3], ftype = filter[4], use_filtfilt = True)
    
    Fs = int(float(rm.get_param('sampling_frequency')))
    width = int(width * Fs)
    bad_samples = dict()
    ThresholdsDict = dict()
    AllThresholdsDict = dict()
    for ch in channels:
        c = get_microvolt_samples(rm, ch)
        c -= c.mean()
        
        Pmsc, freqs = Power(c, width, rng, Fs)
        # przesunięcie o połowę szerokości okna i ponowne obliczenie
        Pmsc_2, freqs = Power(np.roll(c, -width / 2), width, rng, Fs)
        
        # obliczenie progu dla std
        PP = np.concatenate([Pmsc, Pmsc_2])
        if outpath:
            pp_for_hist = PP.copy()
        s = np.inf
        while PP.std() < s:
            s = PP.std()
            PP = PP[PP < StdThr * s + np.median(PP)]
        
        Thresholds = (AbsThr, MedThr * np.median(PP), StdThr * s + np.median(PP))
        Threshold = min(Thresholds)  # wybieramy najostrzejszy (najnizszy) prog
        ThresholdType = ['abs', 'med', 'std'][np.argmin(Thresholds)]
        print "{:>4}(muscles) - abs: {:.3g}, med: {:.3g}, std: {:.3g}, chosen: {}".format(*(ch,) + Thresholds + (ThresholdType,))
        
        if Threshold <= 0:
            raise Exception("Threshold = {}\nYou must give some threshold in range (0,+oo)!".format(Threshold))
        
        bad_segment = Pmsc * (Pmsc > Threshold)  # jednostronnie, bo interesują nas tylko przypadki o zbyt dużej, a nie zbyt małej mocy
        bad_segment_2 = Pmsc_2 * (Pmsc_2 > Threshold)  # jednostronnie
        
        ThresholdsDict[ch] = (Threshold, ThresholdType)
        AllThresholdsDict[ch] = Thresholds
        bad_samples[ch] = np.max([BadSeg2BadSamp(bad_segment, width, c.size), BadSeg2BadSamp(bad_segment_2, width, c.size, width / 2)], axis = 0)

        if outpath:
            bins = max(20, pp_for_hist.size // 100)
            pos = map1020[ch]
            ax = axs[pos.y, pos.x]
            pp_for_hist[pp_for_hist > 3*Threshold] = 3*Threshold  # trzeba przyciąć takie przypadki, bo potrafią rozwalić cały histogram
            ax.hist(pp_for_hist, bins = bins)
            mx = ax.get_ylim()[1]
            for i in range(3):
                if Thresholds[i] < np.inf:
                    ax.plot([Thresholds[i], Thresholds[i]], [0, mx], label = artifact_labels[i])
            ax.plot([Threshold], [mx], "o", label = ThresholdType)
            ax.set_title("{} {}".format("muscles", ch))
            set_axis_fontsize(ax, fontsize * figure_scale / 8)
            ax.legend(fontsize = fontsize * figure_scale / 8)

    
    if outpath:
        fig.savefig(outpath.strip(".obci.tag")+"_muscles.png")
        plt.close(fig)
    
    if full_output:
        return bad_samples, AllThresholdsDict
    else:
        return bad_samples, ThresholdsDict


def BadSeg2BadSamp(bad_segments, width, size, shift = 0):
    y = np.zeros(size, dtype = bad_segments.dtype)
    for i in range(bad_segments.size):
        y[i * width + shift:(i + 1) * width + shift] = bad_segments[i]
    return y


def ArtTagsWriter(bad_samples, Fs, all_channels, MergWin, ArtName = 'artifact', Thresholds = np.inf, MinDuration = 0.03):
    tags = []
    for ch in bad_samples:
        if Thresholds == np.inf:
            threshold, threshold_type = np.inf, 'art'  # default
        else:
            threshold, threshold_type = Thresholds[ch]
        
        bads = bad_samples[ch]
        
        if DEBUG:
            timeline = 1. * np.arange(bads.size) / Fs
        
        start = stop = 0.
        artifact = False
        value = -np.inf
        for i in range(bads.size):
            if bads[i]:
                if bads[i] > value:
                    value = bads[i]
                stop = i
                if not artifact:
                    start = i
                    artifact = True
            elif artifact:
                if i - stop > MergWin * Fs:
                    if stop - start < int(MinDuration * Fs):  # symetryczne rozszerzenie tagu, żeby nie był węższy niż MinDuration
                        Ext = int(MinDuration * Fs) - stop + start
                        stop += Ext / 2
                        start -= Ext / 2
                    if all_channels.index(ch) == -1:
                        print all_channels
                        print ch
                        raise IndexError("Bad channelNumber!")
                    tag = {'channelNumber': all_channels.index(ch), 'start_timestamp': 1. * start / Fs, 'name': ArtName, 'end_timestamp': 1. * stop / Fs,
                           'desc': {'thr': "{}".format(int(threshold)), 'val': "{}".format(int(value)), 'typ': threshold_type}}
                    tags.append(tag)
                    value = -np.inf
                    artifact = False
                    
                    if DEBUG and timeline[start] > 2 and tag['name'] == "slow" and ch == "Fp1":
                        plt.figure()
                        plt.plot(timeline[start:stop], bads[start:stop])
                        plt.title("{} {}".format(ch, tag['start_timestamp']))
                        
    if DEBUG:
        plt.show()

    return tags


def ArtTagsSaver(artifacts_file_path, tags, rej_dict):
    try:
        os.makedirs(os.path.dirname(artifacts_file_path))
    except OSError:
        pass
    
    writer = TagsFileWriter(artifacts_file_path)
    for tag in tags:
        writer.tag_received(tag)
    writer.finish_saving(0.0)
    
    rej_dict_file = open(artifacts_file_path[0:-9] + '_artifact_rej_dict.json', 'w')
    json.dump(rej_dict, rej_dict_file, indent = 4, sort_keys = True)
    rej_dict_file.close()


def MiniMax(s, w, ch = ''):
    mm = []
    for i in range(0, s.size, w):
        m = s[i:i + w].max() - s[i:i + w].min()
        mm.append(m)
    return np.array(mm)
