# coding: utf-8
import sys

from artifacts_mark import get_dirty_epochs_ids, getDefaultArtRejDict
from mne_conversions import read_manager_to_mne, mne_to_rm_list


def artifacts_rejection(epochs_list_local, start_offset, clean_epochs_list,
                        artifact_tags, eeg_rm = None, drop_chnls = None, all_channels = None, silent = False, outpath = ""):
    
    dirty_epochs_ids = get_dirty_epochs_ids(epochs_list_local, artifact_tags,
                                            eeg_rm = eeg_rm, all_channels = all_channels, art_hist_outpath = outpath)
    
    mne_epochs, tag_type_slices = read_manager_to_mne(epochs = epochs_list_local, baseline = start_offset)
    mne_epochs.drop(dirty_epochs_ids.keys())
    art_rej_dict = getDefaultArtRejDict()
    
    clean_epochs_list_local = mne_to_rm_list(mne_epochs)
    
    print "Number of epochs types:", len(clean_epochs_list_local)
    for i in range(len(clean_epochs_list_local)):
        print "clean epochs: {} of {} stimulus".format(len(clean_epochs_list_local[i]), len(epochs_list_local[i]))
    #    raw_input("Press Enter to continue") #DEBUG
    
    for epn, clean_epochs_local in enumerate(clean_epochs_list_local):
        clean_epochs_list[epn] += clean_epochs_local
    return art_rej_dict
