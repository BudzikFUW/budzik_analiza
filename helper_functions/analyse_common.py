# coding: utf8
import os
import json

import numpy as np

from mne_conversions import read_manager_to_mne

from obci.analysis.obci_signal_processing import read_manager
from obci.analysis.obci_signal_processing.tags.tags_file_reader import TagsFileReader

from helper_functions import get_tagfilters
from helper_functions import fit_eog_ica, remove_ica_components, show_eog_ica
from helper_functions import PrepareRM, GetEpochsFromRM, undrop_channels
from helper_functions import align_tags, shift_tags_relatively_to_signal_beginnig
from artifacts_mark import ArtTagsSaver, ValidateChannels_Artifacts, ValidateChannels_Noise

from artifacts_rejection import artifacts_rejection
import pickle

from config import bad_chnls_db


def check_montage_possibility(montage, drop_chnls):
    """Sprawdza, czy kanały potrzebne do wykonania montaży nie znajdują się aby na liście kanałów do odrzucenia.

    :param montage: typ montażu do sprawdzenia
    :param drop_chnls: lista kanałów do odrzucenia
    :return: True or False
    """
    if montage[0] == 'ears' and "M1" in drop_chnls and "M2" in drop_chnls:
        print "Brak kanałów usznych! Nie można wykonać tego montażu."
        return False
    
    elif montage[0] == 'custom' and np.any([ch in drop_chnls for ch in montage[1:]]):
        print "Brak wymaganych kanałów do zrobienia montażu!"
        print "Lista złych kanałów:", drop_chnls
        print "Żądany montaż", montage
        return False
    
    else:
        return True


def do_analyse(filename, start_offset, duration,
               outdir = "~/results/unknown/noname",
               blok_type = None,
               tag_name = None,

               filters = ([[2, 29.6], [1, 45], 3, 20, "cheby2"]),
               montage = ('car',),
               drop_chnls = None,
               bad_chnls_method = 'drop',

               shift_tags_rel = 0,
               tag_correction_chnls = None,
               correction_window = (-0.1, 0.3),
               tag_offset = 0.0,
               correction_reverse = False,
               correction_thr = None,

               decimate_factor = None,

               remove_eog = None,
               reject_artifacts = False,

               fully_automatic = False,

               ica_manual = True,
               silent = False,

               ears_always = False,
               validate_channels_noise = True):
    
    outdir = os.path.expanduser(outdir)
    try:
        os.makedirs(outdir)
    except OSError:
        pass
    try:
        os.makedirs(os.path.join(outdir, 'clean_epochs'))
    except OSError:
        pass
    try:
        os.makedirs(os.path.join(outdir, 'ica_and_artifacts'))
    except OSError:
        pass
    icamaps_outdir = os.path.join(outdir, 'ica_and_artifacts', 'ica_maps')
    try:
        os.makedirs(icamaps_outdir)
    except OSError:
        pass
    
    fname = filename[:-4]
    eeg_rm = read_manager.ReadManager(fname + '.xml', fname + '.raw', fname + '.tag')

    if not drop_chnls:  # kanały, które nie zostaną użyte, bo nie zawierają zapisu EEG
        drop_chnls = ['l_reka', 'p_reka', 'l_noga', 'p_noga', 'haptic1', 'haptic2',
                      'phones', 'Driver_Saw', 'Saw', 'Sample_Counter', 'TSS']
    
    if fully_automatic:
        ica_manual = False
        silent = True
#        bad_chnls_method = "find_and_drop"

    tags_function_list, epoch_labels = get_tagfilters(blok_type)
    
    bad_chnls = []
    if 'drop' in bad_chnls_method:
        with open(bad_chnls_db) as bad_chnls_db_f:
            bad_chnls_dict = json.load(bad_chnls_db_f)
            try:
                bad_chnls = bad_chnls_dict[os.path.basename(filename)]
            except KeyError:
                print("W magazynie (pliku json) nie udało się znaleźć kanałów oznaczonych, jako wadliwe. Rozważ wykonanie preanalizy.")

    if ears_always:
        if "M1" in bad_chnls:
            bad_chnls.remove("M1")
        if "M2" in bad_chnls:
            bad_chnls.remove("M2")
    
    if bad_chnls_method == "find_and_drop":
        if validate_channels_noise:
            not_mounted_rm = PrepareRM(eeg_rm, drop_chnls + bad_chnls, decimate_factor, gains = True)
            ch_validation = ValidateChannels_Noise(not_mounted_rm)
            del not_mounted_rm
        
            for ch in ch_validation:
                if not ch_validation[ch]:
                    bad_chnls.append(ch)

        if ears_always:
            if "M1" in bad_chnls:
                bad_chnls.remove("M1")
            if "M2" in bad_chnls:
                bad_chnls.remove("M2")
        
        if not check_montage_possibility(montage, bad_chnls):
            print "Brak wymaganych kanałów {}! Nie można wykonać tego montażu.".format(montage[1:])
            if fully_automatic:
                print "Użyję CAR."
                montage = ['car']
            else:
                ans = raw_input("Czy zrobić CAR? (T/n)")
                if ans.lower() == "n":
                    return
                else:
                    montage = ['car']
        
        not_filtered_mounted_rm = PrepareRM(eeg_rm, drop_chnls + bad_chnls, decimate_factor, [], montage, gains = True)
        
        ch_validation = ValidateChannels_Artifacts(not_filtered_mounted_rm, priviliged_channels = ['Fpz', 'Fp1', 'Fp2'])
        
        del not_filtered_mounted_rm  # już nie będzie potrzebny
        
        for ch in ch_validation:
            if not ch_validation[ch]:
                bad_chnls.append(ch)
    
    bad_chnls = list(set(bad_chnls))  # usunięcie z listy powtarzających się kanałów
    if ears_always:
        if "M1" in bad_chnls:
            bad_chnls.remove("M1")
        if "M2" in bad_chnls:
            bad_chnls.remove("M2")
    
    if not check_montage_possibility(montage, bad_chnls):
        print "Brak wymaganych kanałów {}! Nie można wykonać tego montażu.".format(montage[1:])
        if fully_automatic or silent:
            print "Użyję CAR."
            montage = ['car']
        else:
            ans = raw_input("Czy zrobić CAR? (T/n)")
            if ans.lower() == "n":
                return
            else:
                montage = ['car']

    # sprawdzenie ile kanałów pozostało do analizy
    temp_rm = PrepareRM(eeg_rm, drop_chnls = drop_chnls + bad_chnls, montage = montage)
    if int(temp_rm.get_param('number_of_channels')) < 6:
        print "Za mało kanałów EEG, aby kontynuować analizę (mniej niż 6)"
        return
    del temp_rm

    # jeśli nie ma eog, to robimy kanał eog nazwany Fpx, który jest średnią z kanałów Fp1, Fpz i Fp2
    if "eog" not in eeg_rm.get_param('channels_names'):
        chnames = eeg_rm.get_param('channels_names')
        chnames.append("Fpx")
        samples = eeg_rm.get_samples()
        gains = eeg_rm.get_param('channels_gains')
        Fpx = np.zeros(samples.shape[1])
        n = 0
        for ch in ("Fp1", "Fpz", "Fp2"):
            if ch in chnames:
                n += 1
                ind = chnames.index(ch)
                gain = float(gains[ind])
                Fpx += samples[ind] * gain
    
        Fpx /= n * gain
        samples = np.vstack((samples, Fpx))
        gains.append(u"{}".format(gain))
    
        eeg_rm.set_samples(samples, chnames)
        eeg_rm.set_param('channels_gains', gains)
    
        chs_offsets = eeg_rm.get_param('channels_offsets')
        chs_offsets.append(u"0.0")
        eeg_rm.set_param('channels_offsets', chs_offsets)


    # hash calculating
    h = str(hex(hash(
        str(montage) + str(remove_eog) + str(decimate_factor) + str(blok_type)
        + str(bad_chnls_method) + str(reject_artifacts) + str(bad_chnls)
        + str(tag_correction_chnls) + str(tag_offset) + str(filters)
        + str(correction_window) + str(correction_thr) + str(correction_thr)
        + str(start_offset) + str(duration)
    
    )))[-8:]
    
    fiffilename = os.path.basename(fname) + '_mont-{}_btype-{}_h-{}_clean_epochs-epo.fif'.format(montage, blok_type, h)
    fiffilepath = os.path.join(outdir, 'clean_epochs', fiffilename)
    
    print "Cannot use cache {}".format(fiffilepath)
    print "Trying to clean epochs..."
    
    print 'blok type', blok_type, 'montage', montage
    clean_epochs_list = [list() for _ in tags_function_list]  # lista czystych odcinków
    # clean epochs list - list of smart tag lists,
    # first dimension is for every smarttag filtering function,
    # second for every epoch

    if shift_tags_rel != 0:
        shift_tags_relatively_to_signal_beginnig(eeg_rm, shift_tags_rel)
    if tag_correction_chnls:
        align_tags(eeg_rm, tag_correction_chnls,
                   start_offset = correction_window[0],
                   duration = correction_window[1],
                   thr = correction_thr,
                   reverse = correction_reverse,
                   offset = tag_offset)
    
    if 'drop' in bad_chnls_method:
        drop_chnls = list(set(drop_chnls + bad_chnls))
        print '\n\n\nWILL DROP CHNLS', drop_chnls
    
    mounted_rm = PrepareRM(eeg_rm, drop_chnls, decimate_factor, filters, montage, gains = True)
    
    # eeg_rm wciąż zawiera oryginalne dane
    # mounted_rm zawiera sygnał dodatkowo przefiltrowany i zmontowany, bez kanałów z drop_chnls i ewentualnie po decymacji
    if remove_eog:
        for rm_eog in remove_eog:  # wyszukanie pierwszego dostępnego kanału z listy kanałów eog, do użycia w ICA
            if rm_eog in eeg_rm.get_param('channels_names') and rm_eog not in drop_chnls:
                remove_eog = rm_eog
                break
    
    if remove_eog and remove_eog in eeg_rm.get_param('channels_names'):
        icacache_dir = os.path.join(outdir, 'ica_and_artifacts')
        icacache_name = os.path.basename(fname) + str(montage) + str(hash(str(mounted_rm.get_param('channels_names')) + str(filters) +
                                                                          str(montage) + str(remove_eog) + str(drop_chnls))) + '_ICA_CLEANED'
        path = os.path.join(icacache_dir, icacache_name)
        
        try:
            print "ŁADUJĘ ICA Z CACHE:\n", path
            with open(path + '.pickle') as f:
                ica, bads, eog_events = pickle.load(f)
            print "Udało się załadować ICA z cache"

            if ica_manual:  # pokaż wczytany z cache  rozkład na komponenty
                bads = show_eog_ica(mounted_rm, ica, remove_eog, results_path = path)
            
                with open(path + '.pickle', 'w') as f:
                    pickle.dump((ica, bads, eog_events), f)
        except IOError:
            print "BRAK ICA CACHE - uruchamiam ICA"
            ica, bads, eog_events = fit_eog_ica(mounted_rm, remove_eog, manual = ica_manual, montage = montage, ds = fname, outdir = icamaps_outdir)
            try:
                os.makedirs(icacache_dir)
            except OSError:
                pass
            with open(path + '.pickle', 'w') as f:
                pickle.dump((ica, bads, eog_events), f)

        # przygotowanie rm do zapisu do pliku do przyszłego przeglądania
        # rm jest uzupełniany o porzucone kanały (ich wstępne porzucenie jest potrzebne do zrobienia właściwego montażu CAR)
        # i jest jedynie zmontowany, ale nie jest filtrowany, ani nie ma uwzględnionych gains
        mounted_notfiltered_rm = PrepareRM(eeg_rm, drop_chnls, decimate_factor, montage = montage, gains = False)
        clean_mounted_notfiltered_rm = remove_ica_components(mounted_notfiltered_rm, ica, bads, eog_events, silent = True)
        clean_mounted_notfiltered_undropped_rm = undrop_channels(clean_mounted_notfiltered_rm, eeg_rm)
        clean_mounted_notfiltered_undropped_rm.save_to_file(icacache_dir, icacache_name)
        # dalej nie będą potrzebne:
        del clean_mounted_notfiltered_undropped_rm, clean_mounted_notfiltered_rm, mounted_notfiltered_rm

        # przygotowanie rm do dalszej analizy
        mounted_notfiltered_rm = PrepareRM(eeg_rm, drop_chnls, decimate_factor, montage = montage, gains = True)
        clean_mounted_notfiltered_rm = remove_ica_components(mounted_notfiltered_rm, ica, bads, eog_events, silent = True)
        clean_mounted_rm = remove_ica_components(mounted_rm, ica, bads, eog_events, silent = silent, montage = montage, ds = fname, outdir = icamaps_outdir)
        del mounted_notfiltered_rm
    else:
        clean_mounted_notfiltered_rm = PrepareRM(eeg_rm, drop_chnls, decimate_factor, montage = montage, gains = True)
        clean_mounted_rm = mounted_rm
    
    
    epochs_list_local = GetEpochsFromRM(clean_mounted_rm, tags_function_list, start_offset, duration, tag_name)  # lista epok dla bieżącego pliku
    del clean_mounted_rm

    if sum([len(i) for i in epochs_list_local]) == 0:
        print "UWAGA! Brak odcinków dla tych typów bodźców."
        return
    

    h = str(hex(hash(
        str(montage) + str(remove_eog) + str(decimate_factor)
        + str(bad_chnls_method) + str(reject_artifacts) + str(drop_chnls)
    )))[-8:]
    artifacts_file_path = os.path.join(outdir, 'ica_and_artifacts', "{}_{}_{}_{}".format(os.path.basename(fname).split(".")[0],
                                                                                         montage, h, "artifacts.obci.tag"))
    list_of_all_channels = eeg_rm.get_param('channels_names')

    try:
        reader = TagsFileReader(artifacts_file_path)
        artifact_tags = reader.get_tags()
        artifacts_rejection(epochs_list_local, start_offset, clean_epochs_list, artifact_tags,
                                           silent = silent, outpath = artifacts_file_path)
        print "Znaleziono plik z tagami artefaktów.", artifacts_file_path, "\nPrzystępuję do odrzucania odcinków"
    
    except IOError:
        print "Brak pliku z tagami artefaktów lub plik uszkodzony"
        print "Przystępuję do generacji nowego pliku..."
        artifact_tags = []

        art_rej_dict = artifacts_rejection(epochs_list_local, start_offset, clean_epochs_list, artifact_tags,
                                           clean_mounted_notfiltered_rm, drop_chnls, list_of_all_channels, silent = silent, outpath = artifacts_file_path)
        
        signal_length = float(eeg_rm.get_param("number_of_samples")) / float(eeg_rm.get_param("sampling_frequency"))

        for ch in bad_chnls:
            artifact_tags.append({'channelNumber': list_of_all_channels.index(ch), 'start_timestamp': 0, 'name': 'bad_chnl', 'end_timestamp': signal_length,
                                  'desc': {}})
    
        print "Zapis wygenerowanych tagów artefaktów do pliku", artifacts_file_path
        print artifact_tags
        ArtTagsSaver(artifacts_file_path, artifact_tags, art_rej_dict)  # zapis tagów artefaktów do pliku

    if not reject_artifacts:  # w takim przypadku do listy czystych odcinków wrzucamy wszystko
        clean_epochs_list = [list() for _ in tags_function_list]
        for nreps, eps in enumerate(epochs_list_local):
            clean_epochs_list[nreps].extend(eps)
    
    len_list = [len(ce) for ce in clean_epochs_list]
    print 'Found clean epochs in defined groups:', len_list
    
    if 0 not in len_list:
        # caching clean epochs for all files
        clean_mne, tag_type_slices = read_manager_to_mne(epochs = clean_epochs_list, baseline = start_offset, epoch_labels = epoch_labels)
        clean_mne.save(fiffilepath)
    else:
        if len_list[0] == 0:
            print "Lack of epochs for target (or dewiant)"
        if len_list[1] == 0:
            print "Lack of epochs for nontarget (or standard)"
