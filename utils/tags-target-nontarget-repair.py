#!/usr/bin/python

import os,sys,shutil

for root,dirs,files in os.walk(sys.argv[1]):
    for filename in files:
        if "obci.tag" in filename and not "tag.bak" in filename and not ".etr." in filename:
            path=root+"/"+filename
            try: shutil.copy(path,path+".bak")
            except:
                print "PERMISION DENIED:", path
                continue
            print path
            file=open(path,'r')
            lines=[]
            for line in file.readlines():
                line=line.replace("<tag>nontarget</tag>","<type>nontarget</type>")
                line=line.replace("<tag>target</tag>","<type>target</type>")
                line=line.replace("</tag>","</tag>\n")
                line=line.replace("</tag>\n\n","</tag>\n")
                line=line.replace(" />"," />\n")
                line=line.replace(" />\n\n"," />\n")
                lines.append(line)
            file=open(path,'w')
            file.writelines(lines)

