#!/usr/bin/python
#coding: utf-8
from scipy.signal import filtfilt, butter, freqz, iirdesign
import numpy as np
import pylab as py
 
# częstość próbkowania
Fs = 1024.0
# projekt filtru pasmowo przepustowego



#przygotowanie filtrów wedle uznania uzytkownika

line_fs = [50., 100., 150, 200, 250, 300, 350, 400,450,500]
filtry = [[1, 0.5, 3, 20, "cheby2"]]
filtry = []


for line_f in line_fs: 
    filtry.append(
                   [[line_f-1.5, line_f+1.5], [line_f-0.001, line_f+0.001], 1.0, 25., "cheby2"]
                  )

filtry = [
        [[1.5, 20], [0.1, 35], 3, 12, "butter"]]
filtry = [
        [[0.5, 60], [0.1, 85], 3, 12, "butter"], [[50-1.5, 50+1.5], [50-0.001, 50+0.001], 1.0, 25., "cheby2"]]
filtry = [
          [[0.8, 30], [0.01, 45], 3, 12, "butter"],
          [[50-1.5, 50+1.5], [50-0.001, 50+0.001], 1.0, 25., "cheby2"]]
          
filtry = [[0.5, 0.25, 3, 6.97, "butter"],
          [128, 192, 3, 12, "butter"]]

line_fs = [50., 100., 150, 200]
line_fs = [50., 100.]
for line_f in line_fs: 
    filtry.append(
                   [[line_f-2.5, line_f+2.5], [line_f-0.1, line_f+0.1], 3.0, 25., "cheby2"]
                  )

filtry = [[1., 0.5, 3, 6.97, "butter"],  # górnoprzepustowy
           [10, 20, 3, 12.3, "butter"],  # dolnoprzepustowy
           [[47.5, 52.5], [49.9, 50.1], 3, 25, "cheby2"]]  # pasmowy (sieć 50 Hz)

#filtry = [[1., 0.5, 3, 6.97, "butter"],
#           [30, 60, 3, 12.4, "butter"],
#           [[47.5,   52.5], [ 49.9,  50.1], 3, 25, "cheby2"]] 

###########################################
###########################################

# generujemy sygnał
d=10 # dzielnik - im większa wartość, tym mniej punktów sygnału

t = np.arange(0,Fs/2/d,1/Fs)
s = np.sin(np.pi*t*t*d)
y=s.copy() 


H=1+0j #transmitancja
for filtr in filtry:
	wp, ws, gpass, gstop, ftype = filtr

	nyquist = Fs/2.
	wp=np.array(wp)/nyquist
	ws=np.array(ws)/nyquist


	b,a = iirdesign(wp, ws, gpass, gstop, analog = False, ftype=ftype, output = "ba")
	
	print b.shape
	print a.shape
	print
	
	# obliczamy funkcję przenoszenia
	W,h = freqz(b,a,1*int(Fs))
	H*=h

	# Filtrowanie z zerowym opoznieniem fazowym
	y = filtfilt(b,a,y)

transmitancja = np.abs(H)
 
#opóźnienie grupowe
grupowe = -np.diff(np.unwrap(np.angle(H)))/np.diff(W)  
 
# przeliczamy skalę częstości na Hz 
f = W/(np.pi)*Fs/2.0
 



# WYKRESY
py.figure()
py.subplot(311)
py.plot(f, 20*np.log10(transmitancja))  # przeliczenie modułu transmitancji na dB
py.title('transmitancja')
py.xlabel('[Hz]')
py.ylabel('[dB]')
 
py.subplot(312)
py.plot(f[:-1], grupowe )
py.title('opoznienie grupowe')
py.xlabel('[Hz]')
py.ylabel('punkty')
 
py.subplot(313)
py.plot(t*d,s)
py.plot(t*d,y,'r')
py.legend(('s','filtfilt'))
py.xlabel('f[Hz]')
py.title('sygnaly')

py.show()
