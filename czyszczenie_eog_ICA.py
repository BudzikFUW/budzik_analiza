# coding: utf8
import os, sys
import json

from helper_functions import montage_ears, remove_eog_ica, get_microvolt_samples, mgr_filter, exclude_channels
from obci.analysis.obci_signal_processing import read_manager
import matplotlib
from config import OUTDIR

havedisplay = "DISPLAY" in os.environ
if not havedisplay:
    matplotlib.use('Agg')

from analyse_common import get_filelist, do_analyse

bad_chnls_db = 'bad_channels_db.json'


def load_json_bads(ds):
    with open(bad_chnls_db) as bcdbf:
        bad_channels_dict = json.load(bcdbf)
        try:
            bad_chnls = bad_channels_dict[os.path.basename(ds) + '.raw']
        except KeyError:
            bad_chnls = []
    return bad_chnls


def czyszczenie_ica(ds):
    #    montages = [['csa']] #rodzaje montażu (wyniki zostaną obliczone kolejno dla każdego z listy)
    remove_eog = "eog"  #
    #   remove_eog = None    #
    ears = ['M1', 'M2']  # only both
    # ~ filters = [
    # ~ [[1.5, 10], [0.5, 15], 3, 20, "cheby2"]]  # tu można podać kilka różnych filtrów, zostaną zastosowane kolejno
    # ~ filters = [
    # ~ [[1.5, 20], [0.1, 35], 3, 12, "butter"]]
    # filters = [
    #      [[0.5, 30], [0.01, 45], 3, 12, "butter"]]
    # filters = [
    #    [[2, 29.6], [1, 45], 3, 20, "cheby2"]]  # tu można podać kilka różnych filtrów, zostaną zastosowane kolejno
    line_fs = [50., 100., 150, 200, 250, 300, 350, 400, 450, 500]
    # ~ filters = [[1, 0.5, 3, 20, "cheby2"]]
    # for line_f in line_fs:
    #    filters.append(
    #                   [[line_f-1.0, line_f+1.0], [line_f-0.1, line_f+0.1], 1.0, 25., "cheby2"]
    #                  )
    # filters = [
    #    [[0.5, 60], [0.1, 85], 3, 12, "butter"], [[50-1.5, 50+1.5], [50-0.001, 50+0.001], 1.0, 25., "cheby2"]]
    
    filters = [
        [[0.8, 30], [0.01, 45], 3, 12, "butter"],
        [[50 - 1.5, 50 + 1.5], [50 - 0.001, 50 + 0.001], 1.0, 25., "cheby2"]]
    
    drop_chnls = [u'l_reka', u'p_reka', u'l_noga', u'p_noga', u'haptic1', u'haptic2',
                  u'phones', u'Driver_Saw', u'Saw',
                  u'Sample_Counter']  # kanały, które nie zostaną użyte, bo nie zawierają zapiu EEG
    
    drop_chnls = drop_chnls + load_json_bads(ds)
    
    print '\nWILL DROP CHNLS', drop_chnls
    
    eeg_rm = read_manager.ReadManager(ds + '.xml', ds + '.raw', ds + '.tag')
    
    eeg_rm.set_samples(get_microvolt_samples(eeg_rm), eeg_rm.get_param('channels_names'))
    params = eeg_rm.get_params()
    
    eeg_rm.set_param('channels_gains', [unicode(1.0) for i in params['channels_gains']])
    
    eeg_rm = exclude_channels(eeg_rm, drop_chnls)
    
    for filter in filters:
        print "Filtering...", filter
        eeg_rm = mgr_filter(eeg_rm, filter[0], filter[1], filter[2],
                            filter[3], ftype = filter[4], use_filtfilt = True)
    eeg_rm = montage_ears(eeg_rm, ears[0], ears[1], exclude_from_montage = drop_chnls + ['eog'])
    
    eeg_rm, eog_events = remove_eog_ica(eeg_rm, remove_eog, montage = 'ears-' + str(ears), ds = ds, manual = True)
    
    eeg_rm.save_to_file(OUTDIR, os.path.basename(ds) + '_EOG_ICA')


def main(argv):
    try:
        ds = argv[1][:-4]
    except IndexError:
        raise IOError('Please provide path to .raw')
    czyszczenie_ica(ds)


if __name__ == '__main__':
    main(sys.argv)
