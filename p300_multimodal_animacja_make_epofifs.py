#!/usr/bin/env python2
# coding: utf8
import os
import sys

import matplotlib
from helper_functions.analyse_common import do_analyse
from helper_functions.helper_functions import get_filelist
from config import create_outdir_name



def p300_multimodal_animacja(argv, fully_automatic = True):
    montages = [['ears', "M1", "M2"]]  # rodzaje montażu (wyniki zostaną obliczone kolejno dla każdego z listy)
    
    start_offset = -0.2
    duration = 1.
    
    # tagi z paradygmatów o dwóch rodzajach bodźców rozpoznajemy po typach
    blok_types = ['multimodal_both_movie']  # osobny plik -epo.fif dla każdego typu z listy
    
    # korekcja ICA metodą korelacji z pierwszym dostępnym kanałem z listy:
    remove_eog = ["eog", "Fpx"]
    
    # tu można podać kilka różnych filtrów, zostaną zastosowane kolejno:
    #    filters = [[[2, 29.6], [1, 45], 3, 20, "cheby2"]]      # filtr all-in-one
    filters = [[1., 0.5, 3, 6.97, "butter"],  # górnoprzepustowy
              [10, 20, 3, 12.3, "butter"],  # dolnoprzepustowy
              [[47.5, 52.5], [49.9, 50.1], 3, 25, "cheby2"]]  # pasmowy (sieć 50 Hz)
    
    drop_chnls = ['l_reka', 'p_reka', 'l_noga', 'p_noga', 'haptic1', 'haptic2',
                  'phones', 'Driver_Saw', 'Saw', 'Sample_Counter', 'TSS']  # kanały, które nie zostaną użyte, bo nie zawierają zapiu EEG
    
    filelist = get_filelist(argv)
    filelist = [filename for filename in filelist if ".obci.raw" in filename and ".etr." not in filename]
    
    bad_chnls_method = 'drop'  # drop or find_and_drop
    
    # każdy plik, typ bloku i montaż są analizowane osobno, więc dla każdego zestawu powstanie osobny plik -epo.fif
    for filepath in filelist:
        outdir = create_outdir_name(filepath)
        
        for blok_type in blok_types:
            for montage in montages:
                do_analyse(filepath, start_offset, duration,
                           blok_type = blok_type,
                
                           outdir = outdir,
                
                           filters = filters,
                           montage = montage,
                           drop_chnls = drop_chnls,
                           bad_chnls_method = bad_chnls_method,
                
                           remove_eog = remove_eog,
                           reject_artifacts = True,
                           fully_automatic = fully_automatic,
                           
                           ears_always = True,
                           validate_channels_noise = False)


def main(argv):
    try:
        p300_multimodal_animacja(argv[1:])
    except IndexError:
        raise IOError('Please provide path to .raw')


if __name__ == '__main__':
    main(sys.argv)
