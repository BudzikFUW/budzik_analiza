#!/usr/bin/env python2
# coding: utf8

import os
import sys

import matplotlib
havedisplay = "DISPLAY" in os.environ
if not havedisplay:
    matplotlib.use('Agg')
import pylab as pb
import numpy as np

import mne
from helper_functions.mne_conversions import mne_to_rm_list

from helper_functions.helper_functions import get_filelist, evoked_list_plot_smart_tags

from miscellaneous import n_parent_dirpath

default_chnls_to_draw = ('Fp1', 'Fpz', 'Fp2', 'F7', 'F3', 'Fz', 'F4', 'F8', 'T3', 'C3', 'Cz', 'C4', 'T4',
                         'T5', 'P3', 'Pz', 'P4', 'T6', 'O1', 'Oz', 'O2', 'M1', 'M2')

# 1e10 służy jako nieskończoność (nie dawać większej, bo mogą być problemy z błędami obcięcia)

# lista Region Of Interest dla poszczególnych paradygmantów, czyli obszarów, w których szukamy klastrów
# wartości wyrażone w ms
ROIs = {"local": (650, 850),
        "global": (850, 1600),
        "vep": (0, 1000),
        "p300-czuciowe": (200, 900),
        "p300-sluchowe-slowa": (200, 900),
        "p300-sluchowe-tony": (200, 900),
        "p300-wzrokowe": (200, 900)
        }

chnls_to_clusterize = {"local": ('F3', 'Fz', 'F4', 'C3', 'Cz', 'C4', 'P3', 'Pz', 'P4'),
                       "global": ('F3', 'Fz', 'F4', 'C3', 'Cz', 'C4', 'P3', 'Pz', 'P4'),
                       "vep": ('O1', 'Oz', 'O2', 'C3', 'Cz', 'C4', 'P3', 'Pz', 'P4'),
                       "p300-czuciowe": ('F3', 'Fz', 'F4', 'C3', 'Cz', 'C4', 'P3', 'Pz', 'P4'),
                       "p300-sluchowe-slowa": ('F3', 'Fz', 'F4', 'C3', 'Cz', 'C4', 'P3', 'Pz', 'P4'),
                       "p300-sluchowe-tony": ('F3', 'Fz', 'F4', 'C3', 'Cz', 'C4', 'P3', 'Pz', 'P4'),
                       "p300-wzrokowe": ('O1', 'Oz', 'O2', 'C3', 'Cz', 'C4', 'P3', 'Pz', 'P4')
                       }


def erp(filelist, spatiotemporal=False, classification_pipeline_clusters=False):
    # wybieram tylko pliki typu -epo.fif
    fifpath_list = [filepath for filepath in filelist if "-epo.fif" in filepath]
    
    epochs_list = [[], []]
    if not fifpath_list:
        print('Please provide correct path to *-epo.fif')
        exit()

    for fifpath in fifpath_list:
        try:
            mne_epochs = mne.read_epochs(fifpath)
            rm_list = mne_to_rm_list(mne_epochs)
            for i in range(len(rm_list)):
                epochs_list[i] += rm_list[i]
            epoch_labels = sorted(mne_epochs.event_id.keys(), key = lambda e: mne_epochs.event_id[e])
            print "Using clean epochs from file {}".format(fifpath)
        except IOError:
            print('Please provide correct path to *-epo.fif')
            exit()
    
    full_commonprefix = os.path.commonprefix(fifpath_list)
    if "mont-" in full_commonprefix:
        commonprefix = full_commonprefix[:full_commonprefix.index("_mont-")]
    else:
        commonprefix = full_commonprefix

    # commonprefix = commonprefix + '_spat-{}_clasification_pipeline_clusters-{}'.format(spatiotemporal, classification_pipeline_clusters)
    # clasification_pipeline_clusters zostało zarzucone, więc już nie dopisuję tego w nazwie, bo nie ma takich plików

    paradigm_name = os.path.basename(n_parent_dirpath(fifpath_list[0], 4))
    
    results_filename = paradigm_name + ('_spatial' if spatiotemporal else '') + "_" + os.path.basename(commonprefix)

    montages = set()
    btypes = set()
    for fifpath in fifpath_list:
        pos_mont = fifpath.index("mont-")
        pos_btype = fifpath.index("btype-")
        pos_h = fifpath.index("h-")
    
        montage = fifpath[pos_mont + 5: pos_btype - 1]
        btype = fifpath[pos_btype + 6: pos_h - 1]
    
        montages.add(montage)
        btypes.add(btype)

    montages = list(montages)
    btypes = list(btypes)

    if "_h-" in full_commonprefix:
        pos_h = fifpath.index("_h-")
        h = full_commonprefix[pos_h + 3: pos_h + 3 + 8]
        results_filename = os.path.basename(results_filename) + '_{}_{}_{}.png'.format(montages, btypes, h)
    else:
        results_filename = os.path.basename(results_filename) + '_{}_{}.png'.format(montages, btypes)

    outdir = os.path.join(os.path.dirname(os.path.dirname(commonprefix)), "erp")

    try:
        os.makedirs(outdir)
    except OSError:
        pass
    
    if paradigm_name == "global-local":
        paradigm_name = btypes[0]
    
    fig, p_values = evoked_list_plot_smart_tags(epochs_list, show = False,
                                                chnames = default_chnls_to_draw,
                                                chnls_to_clusterize = chnls_to_clusterize[paradigm_name],
                                                size = (20, 5),
                                                labels = epoch_labels,
                                                start_offset = mne_epochs.tmin,
                                                roi = ROIs[paradigm_name],
                                                one_scale = True,
                                                permutation_test = True,
                                                spatiotemporal = spatiotemporal,
                                                classification_pipeline_clusters = classification_pipeline_clusters)
      
    savefileimg = os.path.join(outdir, results_filename)
    signifact_p_values = ", ".join(["{:.2g}".format(p) for p in p_values if p < 0.1])
    suptitle = "{}\n{}".format(results_filename[:-4], signifact_p_values)
    pb.suptitle(suptitle, fontsize = 24)

    fig.savefig(savefileimg)
    pb.close(fig)
    
    np.savetxt(savefileimg[:-4] + "_p_values.txt", p_values)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        raise IOError('Please provide path to *-epo.fif')
    else:
        erp(get_filelist(sys.argv[1:]), spatiotemporal=False)
        # erp(get_filelist(sys.argv[1:]), spatiotemporal=True)
