#!/usr/bin/env python2
# coding: utf8
import os
import sys

from helper_functions.analyse_common import do_analyse
from helper_functions.helper_functions import get_filelist
from config import create_outdir_name


def global_local(argv, fully_automatic = True):
    montages = [['ears', "M1", "M2"]]  # rodzaje montażu (wyniki zostaną obliczone kolejno dla każdego z listy)
    
    start_offset = -0.2
    duration = 1.6
    
    # tagi z paradygmatów o dwóch rodzajach bodźców rozpoznajemy po typach
    blok_types = ['global', 'local']  # osobny plik -epo.fif dla każdego typu z listy
    
    # korekcja ICA metodą korelacji z pierwszym dostępnym kanałem z listy:
    remove_eog = ["eog", "Fpx"]  # Fpx oznacza średnią z dostępnych kanałów z listy: Fp1, Fpz, Fp2 (jeśli któregoś brakuje są brane pozostałe dwa)
    
    tag_correction_chnls = ['phones']
    correction_window = [-0.01, 0.5]
    
    # tu można podać kilka różnych filtrów, zostaną zastosowane kolejno:
    #    filters = [[[2, 29.6], [1, 45], 3, 20, "cheby2"]]      # filtr all-in-one
    filters = [[1., 0.5, 3, 6.97, "butter"],  # górnoprzepustowy
              [10, 20, 3, 12.3, "butter"],  # dolnoprzepustowy
              [[47.5, 52.5], [49.9, 50.1], 3, 25, "cheby2"]]  # pasmowy (sieć 50 Hz)
    
    drop_chnls = ['l_reka', 'p_reka', 'l_noga', 'p_noga', 'haptic1', 'haptic2',
                  'phones', 'Driver_Saw', 'Saw', 'Sample_Counter', 'TSS']  # kanały, które nie zostaną użyte, bo nie zawierają zapiu EEG
    
    filelist = get_filelist(argv)
    filelist = [filename for filename in filelist if ".obci.raw" in filename and ".etr." not in filename]
    
    bad_chnls_method = 'drop'  # drop or find_and_drop
    
    # każdy plik, typ bloku i montaż są analizowane osobno, więc dla każdego zestawu powstanie osobny plik -epo.fif
    if not filelist:
        print 'Please provide path to .obci.raw'
    for filepath in filelist:
        outdir = create_outdir_name(filepath)
        
        for blok_type in blok_types:
            for montage in montages:
                do_analyse(filepath, start_offset, duration,
                           blok_type = blok_type,
                
                           outdir = outdir,
                
                           filters = filters,
                           montage = montage,
                           drop_chnls = drop_chnls,
                           bad_chnls_method = bad_chnls_method,
                           tag_correction_chnls = tag_correction_chnls,
                           correction_window = correction_window,
                
                           remove_eog = remove_eog,
                           reject_artifacts = True,
                           fully_automatic = fully_automatic,
                           
                           ears_always = True,
                           validate_channels_noise = False)


def main(argv):
    global_local(argv[1:])


if __name__ == '__main__':
    main(sys.argv)
