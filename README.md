# BudzikFUW 
Większość skryptów jest napisana w **Pythonie 2**, chyba że zaznaczono inaczej.

Repozytorium zawiera skrypty tworzone do analizy danych zbieranych z pomiarów aktywnych:
* VEP,
* P300 wzrokowe (statyczne i z animacji),
* P300 słuchowe (na tonach oraz na słowach),
* P300 czuciowe,
* Global-Local,
* P300 multimodalne (łączące bodźce wzrokowe i słuchowe),
* ERD/S (motor imagery).

Moduł tworzony w ramach grantu Opus: "Interfejs mózg-komputer do diagnozy i komunikacji w zaburzeniach świadomości" finansowanego przez Naukowego Centrum Nauki. Projek realizowany przez Wydział Fizyki Uniwersytetu Warszawskiego.
Czas realizacji projektu: 2016 - 2018 r.

## 1. Instrukcja instalacji - GNU/Linux
Zainstaluj potrzebne zależności:
```sh
$ sudo apt-get install git python-pip
```
Ściągnij repozytorium do katalogu domowego:
```sh
$ git clone https://gitlab.com/BudzikFUW/budzik_analiza.git
```
Zainstaluj potrzebne zależności wymienione w pliku requirements.txt. W tym celu:
```sh
$ cd budzik_analiza
$ pip install --upgrade pip
$ ./install_dependencies.sh
```

## 2. Ogólny flow analizy dla potencjałów wywołanych
Analiza składa się z trzech części: ręczna preanaliza, przygotowania danych do analizy, analizy.
* Celem preanalizy jest diagnoza jakości sygnału i oznaczenie złych kanałów (nie jest konieczna, ale może poprawić wyniki).
* Przygotowanie danych do analizy sprowadza się do usunięcia mrugnięć metodą ICA, oznaczenia artefaktów, a następnie wybór czystych odcinkow wokoł bodźców.
* Analiza jest w zasadzie dowolna - można liczyć erp z klastrami, spektrogramy, AUC, etc.

Preanaliza (preanalyse.py):
1. Trzy wskaźniki energii dla każdego z kanałów EEG oraz EOG wraz z orientacyjnymi progami:
    * pierwszy wskaźnik – pasmo sieci 
    * drugi wskaźnik – pasmo EEG
    * rzeci wskaźnik – szum
2. Wizualna ocena niezmontowanego sygnału potraktowanego jedynie filtrem pasmowo zaporowym [49.9, 50.1] Hz.
3. Po wizualnej ocenie, w tym samym oknie, należy zaznaczyć wadliwe kanały sugerując się wskaźnikami z punktu 1.

Przygotowanie danych do analizy:
 1. Usuwanie zaznaczonych w preanalizie wadliwych kanałów 
 2. Oznaczanie artefaktów i wyrzucanie kanałów, które są zanieczyszczone w ponad 20%.
 3. Usuwanie mrugnięć metodą usuwania komponenty ICA (obliczonej na całym sygnale) skorelowanej z kanałem EOG, próg 0.25)
 4. Ponowne wyszukiwanie artefaktów na oczyszczonym z mrugnięć sygnale
 5. Podział sygnału na odcinki zgodnie z tagami bodźców wg wybranej funkcji filtrującej zależnej od typu paradygmatu.
 6. Usunięcie odcinków przekrywających się z artefaktami i zapisanie wynikowej listy czystych odcinków do pliku -epo.fif do przyszłego użycia.

Więcej informacji znajdziesz tu: https://docs.google.com/presentation/d/1bKKll8L2r86rC2wMySyx70Outr92NyJAq4m6y_MnYBA/edit#slide=id.p20

Analiza jest w zasadzie dowolne. Wspólne punkty sprowadzają się do:
 1. Wczytania pliku/plików -epo.fif.
 2. Wykonania na nich wybranego przez siebie rodzaju analizy.

W repozytorium dostępne są gotowe skryptu do niektórych rodzajów analizy.
Będą one omówione w kolejnym podrozdziale (po użyciu skryptów przygotowujacych).

### 2.1 Użycie skryptów przygotowujących dane:
W repozytorium znajdują się skrypty przygotowane do wytworzenia plików czystych odcinków wokół bodźców (czystych epok). Produkują one jeden plik -epo.fif dla każdego pliku, montażu i typu bloku:
* VEP (_vep_make_epofifs.py_),
* P300 wzrokowe (_p300_wzrokowe_make_epofifs.py_),
* P300 słuchowe tony (_p300_sluchowe_tony_make_epofifs.py_),
* P300 słuchowe słowa (_p300_sluchowe_slowa_make_epofifs.py_),
* P300 czuciowe (_p300_czuciowe_make_epofifs_),
* Global-Local (_global_local_make_epofifs.py_),
* P300 wzrokowe z animacji (_p300_wzrokowe_animacja_make_epofifs.py_),
* P300 multimodalne (_p300_multimodal_make_epofifs.py_),
* P300 multimodalne z animacji (_p300_multimodal_animacja_make_epofifs.py_),
* ERD/S (erds_make_epofifs.py) - (!)eksperymentalny(!).

W repozytorium znajduje się również skrypt `make_epofifs_for_all_data_in_catalog.py`, który potrafi przejrzeć całe drzewo katalogów wewnątrz podanego katalogu,
i uruchomić dla znalezionych danych właściwy skrypt z listy powyżej. Warunkiem poprawnego działania jest zachowanie konwencji segregowania danych w bazie danych.

Jak widać, dla każdego paradygmatu jest osobny skrypt, w którym znajdują się ustawienia dla niego specyficzne, po czym flow przechodzi do części do_analyse
w skrypcie `helper_functions/analyse_common.py`.
Skrypty radzą sobie z listami plików, wykonując się kolejno dla każdego z nich (żadnego łączenia na tym etapie!).

Przykłady, jak należy uruchamiać poszczególne rodzaje skryptów:
```sh
$ ./preanalyse.py [ścieżka-do-pliku]/dane.obci.raw
$ ./*_make_epofifs.py [ścieżka-do-pliku]/dane.obci.raw
$ ./make_epofifs_for_all_data_in_catalog.py ścieżka-do-katalogu-z-danymi
```
Wyniki zapisywane są w katalogu `/home/nazwa_użytkownika/budzik_results` i są posegregowane na paradygmaty, tury i pacjentów.

### 2.2 Użycie skryptów analizujących dane:
W repozytorium dostępnych jest kilka skryptów wykonujących finalną analizę danych.

#### 2.2.1 Statystyka na klastrach
Skrypt `analyse_erp.py` działa na zbiorze plików _-epo.fif_ dowolnego z paradygmatów.

Użycie:
```sh
$ ./analyse_erp.py [ścieżka-do-pliku]/plik1-epo.fif [ścieżka-do-pliku]/plik2-epo.fif
```
lub, żeby wybrać wszystkie pliki z danego katalogu
```sh
$ ./analyse_erp.py [ścieżka-do-pliku]/*-epo.fif
```

Uwaga! Skrypt ten łączy dane ze wszystkich plików w jeden zbiór dancyh, a dopiero później wykonuje uśrednianie odcinków i oblicza statystykę na klastrach.

Aby wykonać ten rodzaj analizy automatycznie na wszytkich plikach, można wykorzystać skrypt `run_analyse_erp_for_all_epofifs_in_catalog.py`:
```sh
$ ./run_analyse_erp_for_all_epofifs_in_catalog.py ścieżka-do-katalogu-z-plikami-fif
```
Skrypt przegląda całe drzewo katalogów obecnych w podanym katalogu i grupuje znalezione pliki _-epo.fif_ według ich położenia, typu montażu i typu bloku.

Wyniki w postaci wykresów w plikach _*.png_ zapisywane są katalogu _erp_ obok katalogu, który zawiera bezpośrednio pliki _-epo.fif_.
#### 2.2.2 Analiza oparta o BCI (ROC, AUC, etc.)
######todo - Marian Dovgialo - tobe


## 3. Informacje i uwagi dotyczace skryptów
Dla każdego paradygmatu ERP jest osobny skrypt, w którym znajdują się specyficzne dla niego ustawienia (wartości parametrów) podawanych do funkcji do_analyse
w skrypcie helper_functions/analyse_common.py

Funkcja ```do_analyse``` przyjmuje parametry:
```sh
def do_analyse(filename, start_offset, duration,
               outdir = "~/results/unknown/noname",
               blok_type = None,
               tag_name = None,

               filters = ([[2, 29.6], [1, 45], 3, 20, "cheby2"]),
               montage = ('car',),
               drop_chnls = None,
               bad_chnls_method = 'drop',

               shift_tags_rel = 0,
               tag_correction_chnls = None,
               correction_window = (-0.1, 0.3),
               tag_offset = 0.0,
               correction_reverse = False,
               correction_thr = None,

               decimate_factor = None,

               remove_eog = None,
               reject_artifacts = False,

               fully_automatic = False):
```

### 3.1 Skrypt analyse_erds.py
Uwaga! Skrypt ten jest napisany w Python wersji 3, a nie 2, jak pozostałe skrypty.

Skrypt uruchomiony z opcją `-h` (bez żadnych dodatykowych parametrów) wyświetli listę dostępnych opcji.
Jedynym obowiązkowym parametrem jest ścieżka do pliku, czyli np.
```
./analyse_erds.py /moj/katalog/danych/plik.obci.raw
```
spowoduje wyświetlenie spektrogramu i ERDS (bez wyróżnienia obszarów statystycznie istotnych) z podanego pliku.

Skrypt przyjmuje szereg opcjonalnych parametrów:
* `-a` metoda usuwania artefaktów, np. `lowfreq`, domyślnie: żadna
* `-c` klasyfikator do obliczenia "cross-validation accuracy", domyślnie: żaden
* `-m` poprawka wielokrotnych porównań do testu statystycznego, np. `fdr`, domyślnie: żadna
* `-o` katalog do wygenerowania wykresów, domyślnie: `~/results`
* `-r` liczba powtórzeń do testu bootstrap, domyślnie: 2000
* `-s` rodzaj testu statystycznego, np. `bootstrap`, domyślnie: żaden
* `-u` jeśli do estymacji mocy ma być zastosowana metoda Welcha
* `-w` nazwa okna czasowego dla spektrogramu, domyślnie: `hanning`

## 4. Usuwanie artefaktów
Parametry usuwania artefaktów:
* SlopeWindow -  szerokość okna [s], w którym szukamy iglic i stromych zboczy (slope)
* SlowWindow - szerokość okna [s], w którym szukamy (fala wolna) 
* OutliersMergeWin - szerokość okna [s], w którym łączymy sąsiadujące obszary z próbkami outlier
* MusclesFreqRange - przedział częstości [Hz], w którym szukamy artefaktów mięśniowych

Progi powyżej których próbka jest oznaczana jako slope:
* SlopesAbsThr - bezwzględna wartość amplitudy [µV] peak2peak w oknie SlopeWindow
* SlopesStdThr - wartość peak2peak w oknie, jako wielokrotność std
* SlopesThrRel2Med - wartość peak2peak w oknie, jako wielokrotność mediany

Progi powyżej których próbka jest oznaczana jako slow (fala wolna):
* SlowsAbsThr -  bezwzględna wartość amplitudy [µV] peak2peak w oknie SlowWindow
* SlowsStdThr - wartość peak2peak w oknie, jako wielokrotność std
* SlowsThrRel2Med - wartość peak2peak w oknie, jako wielokrotność mediany

Progi powyżej których próbka jest oznaczana jako outlier:
* OutliersAbsThr - bezwzględna wartość amplitudy [µV] (liczona od 0)
* OutliersStdThr- wartość amplitudy, jako wielokrotność std
* OutliersThrRel2Med- wartość amplitudy, jako wielokrotność mediany

Progi powyżej których próbka jest oznaczana jako muscle:
* MusclesAbsThr -  np.inf, [µV²] bezwzględna wartość średniej mocy na próbkę w zakresie częstości MusclesFreqRange, której lepiej nie ustawiać, bo pod tym wzlęgem kanały się bardzo różnią
* MusclesStdThr' - wartość amplitudy, jako wielokrotność std
* MusclesThrRel2Med' - wartość amplitudy, jako wielokrotność mediany

# Twórcy
* Anna Chabuda (@achabuda)
* Marian Dovgialo (@mdov)
* Anna Duszyk (@Duszyk)
* Marcin Pietrzak (@marcinp)
* Piotr Różański (@develancer)
* Magdalena Zieleniewska (@mzielen)
* Piotr Durka (supervisor)