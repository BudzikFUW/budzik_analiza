#!/usr/bin/env python2
# coding: utf8
import os
import sys
import json
from copy import deepcopy

from obci.analysis.obci_signal_processing import read_manager

from helper_functions.helper_functions import get_filelist, BandPower, mgr_filter
from helper_functions.mne_conversions import read_manager_continious_to_mne

from config import bad_chnls_db


def preanalyse(argv, not_preanalysed_only = False, count_only = False, only_less_than_n_bads = 1e100, only_more_than_n_bads = -1):
    """preanalyse - marking bad channels"""
    
    drop_chnls = [u'l_reka', u'p_reka', u'l_noga', u'p_noga', u'haptic1', u'haptic2',
                  u'phones', u'Driver_Saw', u'Saw', u'Sample_Counter', 'TSS', 'eog']  # kanały, które nie zostaną użyte, bo nie zawierają zapiu EEG
    
    filelist = get_filelist(argv)
    
    print filelist
    
    flist = set([filename[:-4] for filename in filelist if '.obci.raw' in filename and '.etr.' not in filename])
    for ds in flist:

        with open(bad_chnls_db) as bcdbf:
            bad_channels_dict = json.load(bcdbf)
            try:
                bad_chnls = bad_channels_dict[os.path.basename(ds) + '.raw']
                print '\nPRESELECTED\n', bad_chnls, "\n"
                if not_preanalysed_only or count_only:
                    return 1
            except KeyError:
                bad_chnls = []
                print '\nNOT PREANALYSED\n'
                if count_only:
                    return 0
        
        eeg_rm = read_manager.ReadManager(ds + '.xml', ds + '.raw', ds + '.tag')

        
        if len(bad_chnls) >= only_less_than_n_bads:
            return 1
        if len(bad_chnls) <= only_more_than_n_bads:
            return 1
        
        channels = eeg_rm.get_param('channels_names')
        
        rm = deepcopy(eeg_rm)
        exclude_channels = drop_chnls  # +bad_chnls
        
        channels = [ch for ch in channels if ch not in exclude_channels]
        width = 1.
        # # Power of grid:
        # GridBand = [49.5, 50.5]
        # GridThresholds = [1e5, 1e6, 1e7]
        # BandPower(rm, channels, width, GridBand, "-", GridThresholds)
        
        # Slow oscilations
        EEGBand = [2, 10]
        EEGThresholds = [1e3, 1e4, 1e5]
        BandPower(rm, channels, width, EEGBand, "-", EEGThresholds)
        
        # Power of EEG
        EEGBand = [10, 40]
        EEGThresholds = [5e1, 5e0, 5e-1]
        BandPower(rm, channels, width, EEGBand, "+", EEGThresholds)

        # Power of Noise
        NoiseBand = [52, 98]
        NoiseThresholds = [1e1, 1e2, 1e3]
        BandPower(rm, channels, width, NoiseBand, "-", NoiseThresholds)
        bad_chnls = select_bads_visually(eeg_rm, bad_chnls, drop_chnls, title = os.path.basename(ds))
        bcdbf = open(bad_chnls_db)
        bad_channels_dict = json.load(bcdbf)
        bcdbf.close()
        bad_channels_dict[os.path.basename(ds) + '.raw'] = bad_chnls
        
        bcdbf = open(bad_chnls_db, 'w')
        json.dump(bad_channels_dict, bcdbf, indent = 2, sort_keys = True)
        bcdbf.close()
        
        return 1


def select_bads_visually(rm, bad_chnls = [], no_plot = [], title = ''):
    rm = deepcopy(rm)
    rm = mgr_filter(rm, [47.5, 52.5], [49.9, 50.1], 3, 25, ftype = "cheby2", use_filtfilt = True)
    ds = read_manager_continious_to_mne(rm)
    ds.drop_channels([str(i) for i in no_plot if str(i) in ds.ch_names])
    ds.info['bads'] = bad_chnls
    #    ds.set_eeg_reference() #montaż CSA
    
    # ds.apply_proj()
    # dict(eeg=1e-8)
    n = len(ds.ch_names)
    ds.plot(scalings = dict(eeg = 5e-4, eog = 1e-3), block = True, show = True, title = title, highpass = 0.5, lowpass = 80, n_channels = n)
    print 'Selected bad chnls', ds.info['bads']
    return ds.info['bads']


def main(argv):
    try:
        preanalyse(argv[1:])
    except IndexError:
        raise IOError('Please provide path to .raw')


if __name__ == '__main__':
    main(sys.argv)
