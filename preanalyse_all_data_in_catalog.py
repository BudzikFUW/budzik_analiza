#!/usr/bin/python2
# coding: utf8

import os
import sys
from preanalyse import preanalyse

command = "./preanalyse.py"
only_not_preanalysed = False
only_less_than_n_bads = 1000
only_more_than_n_bads = -1

count_only = False

resume_at = 0

filepaths_list = []
if len(sys.argv) == 1:
    print "Podaj scieżkę do katalogu zawierającego pliki *.obci.raw"
else:
    for catalog in sys.argv[1:]:
        for root, dirs, files in os.walk(catalog):
            for filename in files:  # przeglądamy rekursywnie wszystkie pliki w katalogu podanym, jako pierwszy argument w linii komend
                if "obci.raw" in filename and ".etr." not in filename:  # szukając plików *.obci.raw, ale nie *.etr.obci.raw
                    path = os.path.join(root, filename)  # ścieżka do pliku danych *.obci.raw
                    # if (("p300" in path or "global" in path) and "multim" not in path
                    #    and "USZKODZONE" not in path and "wadliwe" not in path and "uszkodzone" not in path):
                    if ("erds" in path
                       and "USZKODZONE" not in path and "wadliwe" not in path and "uszkodzone" not in path):
                        # print path
                        filepaths_list.append(path)
filepaths_list.sort()
print "LEN", len(filepaths_list)

licznik = resume_at-1
if licznik < 0:
    licznik = 0
for i, filepath in enumerate(filepaths_list):
    print "\n\nNUMBER {}/{}".format(i+1, len(filepaths_list)), filepath, "\n"

    if i < resume_at-1:
        continue

    # uruchomienie preanalyse dla pliku, do którego prowadzi ścieżka path
    pre = preanalyse([filepath, ], not_preanalysed_only = only_not_preanalysed, count_only = count_only,
                     only_less_than_n_bads = only_less_than_n_bads, only_more_than_n_bads = only_more_than_n_bads)

    if pre is not None:
        licznik += pre
print "licznik/len", licznik, len(filepaths_list)
print "pozostało do preanalizy", len(filepaths_list)-licznik