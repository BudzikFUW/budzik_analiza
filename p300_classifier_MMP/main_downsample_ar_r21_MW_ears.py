#!/usr/bin/env python2
# coding: utf8
from classify_score import boot_strap_aucs

boot_strap_aucs(average=3, output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_ar/wyniki_AR_MW_r21_ears/downsample/results_MW_40_1000_r21_ears', pipeline_type='downsampling')
boot_strap_aucs(average=1, output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_ar/wyniki_AR_MW_r21_ears/downsample/results_MW_40_1000_r21_ears', pipeline_type='downsampling')
