#!/usr/bin/env python2
# coding: utf8
from classify_score import boot_strap_aucs

boot_strap_aucs(average=3, pipeline_type='downsampling',
                output_prefix='~/home_bach/results_for_clusterisation_ears_or_fz_cz_car_exclude_bad_new_balancer_clusterisation_preanalysed_averager_fix_test_downsampler_random')
