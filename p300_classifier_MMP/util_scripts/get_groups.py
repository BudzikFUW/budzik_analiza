import json

from scipy.stats import mannwhitneyu

import numpy
import pandas as pd
from collections import defaultdict
import os
import re

import datetime
import sys
import scipy

N_AVR = 3
MINIMUM_TARGETS = 20
CHANNEL_CRITERION = 'F3;Fz;F4;C3;Cz;C4;P3;Pz;P4'.split(';')
MINIMUM_CHANNELS = 4

N_TARGETS = defaultdict(list)

def proba_to_decision(preds):
    return -numpy.log(1/preds - 1)


def do_hists(preds0, preds1, manwhitneyp, auc, outdir):
    import matplotlib
    matplotlib.use("Agg")
    import pylab as pb
    
    pb.figure(figsize=(10, 10))
    # pb.hist(preds0, color='red', bins=numpy.linspace(0, 1, 20), alpha=0.5)
    # pb.hist(preds1, color='blue', bins=numpy.linspace(0, 1, 20), alpha=0.5)
    pb.hist(preds0, color='red', alpha=0.5, normed=True)
    pb.hist(preds1, color='blue', alpha=0.5, normed=True)

    stat, p = scipy.stats.ttest_ind(preds0, preds1, equal_var=True)
    cohens_d = (numpy.mean(preds0) - numpy.mean(preds1))/numpy.std(numpy.concatenate((preds0-numpy.mean(preds0), preds1-numpy.mean(preds1))), ddof=2)
    pb.title("Distribution of classifier probabilities, red - targets, blue - nontargets\n MannWhitneyU p: {:0.3}, AUC: {:0.3}\nt-test p:{:0.3}, cohens d: {:0.3}".format(manwhitneyp, auc, p, cohens_d))
    pb.savefig(os.path.join(outdir,'dec_hist.png'))


INFO_FILE = '~/tabelka_CRS.csv'
INFO_DATA = pd.read_csv(INFO_FILE)

def get_id(filename):
    return INFO_DATA[INFO_DATA['name'] == filename].id.values[0]
#
# ROUNDS = [[1, datetime.datetime(2015,7,1), datetime.datetime(2016,8,30)],
#           [2, datetime.datetime(2016,9,1), datetime.datetime(2017,3,5)],
#           [3, datetime.datetime(2017,4,25), datetime.datetime(2017,8,1)],
#           [4, datetime.datetime(2017,8,1), datetime.datetime(2019,8,30)],
#          ]


ROUNDS = [[1, datetime.datetime(2015,7,1), datetime.datetime(2016,10,31)],
          [2, datetime.datetime(2016,12,1), datetime.datetime(2017,2,28)],
          [3, datetime.datetime(2017,4,1), datetime.datetime(2017,6,30)],
          [4, datetime.datetime(2017,7,1), datetime.datetime(2017,9,30)],
          [5, datetime.datetime(2017,10,1), datetime.datetime(2019,11,30)],
         ]

def get_date(filename):
    # format 1 (zimbabwe)

    try:
        date = datetime.datetime.strptime(filename.split('_')[-1], '%Y%m%dz%H%M%S')
    except (ValueError, IndexError):
        pass
    try:
        date = datetime.datetime.strptime(filename.split('-')[-1], '%Y%m%dz%H%M%S')
    except (ValueError, IndexError):
        pass

    try:
        date_s = '-'.join(filename.split('_')[-4:])
        date = datetime.datetime.strptime(date_s, '%Y-%b-%d-%H%M')
    except (ValueError, IndexError):
        pass
    try:
        return date
    except NameError:
        raise Exception("Unknown date format!: {}".format(filename))

def get_tura(filename):
    date = get_date(filename)
    for round in ROUNDS:
        if date >= round[1] and date < round[2]:
            return round[0]
    raise Exception('Unknown round: {}, {}'.format(date, filename))



def get_groups(workfolder='.'):
    PARADIGMS = {'p300_wz': 'p300_wz',
                 'p300_cz': 'p300_cz',
                 'p300_sl': ['p300_sl', 'tony'],
                 'p300_sl_tony': 'p300_sl.*tony',
                 'global_local': 'global[-_]local'
                 }

    def get_groups_for_paradigm(paradigm):

        groups = [i.upper() for i in ['control_adult', 'control_children', 'emcs', 'mcs-', 'mcs+', 'uws']]

        data = defaultdict(list)
        full_data = list() # tabelka : ID osoby,
        full_data_headers = ['id', 'diagnosis', 'paradigm', 'measure', 'value', 'date', 'round', 'N_avr_targets', 'N_avr_nontargets']

        for g in groups:
            items = os.listdir('{}/{}'.format(workfolder, g))

            for item in items:
                print item, paradigm, type(paradigm)
                if isinstance(paradigm[1], (list, tuple)):
                    pos_re = paradigm[1][0]
                    neg_re = paradigm[1][1]
                    if re.search(pos_re, item.lower()) is None:
                        print 'SKIPPED'
                        continue
                    if re.search(neg_re, item.lower()):
                        print 'SKIPPED'
                        continue
                else:
                    if re.search(paradigm[1], item.lower()) is None:
                        print 'SKIPPED'
                        continue
                print 'Reading:', item
                with open(os.path.join(workfolder, g, item, 'RESULTS.TXT')) as f:
                    data_p = f.read()
                    try:
                        predicts = numpy.loadtxt(os.path.join(workfolder, g, item, 'predictions.txt'))
                        n_targets = predicts[:,1].sum()
                        N_TARGETS[g].append(n_targets)
                        if predicts[:,1].sum()<MINIMUM_TARGETS/N_AVR:
                            continue
                    except IOError:
                        pass
                    
                    
                    N_targets = -1
                    N_nontargets = -1
                    
                    try:
                        preds = numpy.loadtxt(os.path.join(workfolder, g, item, 'predictions.txt'))
                        N_targets = numpy.sum(preds[:, 1].astype(int) == 0)
                        N_nontargets = numpy.sum(preds[:, 1].astype(int) == 1)
                    except:
                        import traceback
                        traceback.print_exc()
                    
                    try:
                        with open(os.path.join(workfolder, g, item, 'data.json'), 'rb') as json_file:

                            data_json = json.load(json_file)
                            chnames = data_json['chnames']
                            try:
                                min_cluster_p = min(data_json['cluster_p_values'])
                                full_data.append([get_id(item), g, paradigm[0], 'min_cluster_p', min_cluster_p, get_date(item), get_tura(item), N_targets, N_nontargets])

                                
                            except:
                                pass
                            if len(set(chnames).intersection(set(CHANNEL_CRITERION))) < MINIMUM_CHANNELS:
                                print g, item, "skiping - not enough critical channels"
                                continue
                    except (IOError, ValueError):
                        pass



                    try:
                        print(g, item)
                        AUC = float(data_p.splitlines()[6])
                        print('AUC', AUC)
                        data[g+' AUC'].append(AUC)
                        full_data.append([get_id(item), g, paradigm[0], 'AUC', AUC, get_date(item), get_tura(item), N_targets, N_nontargets])

                    except IndexError:
                        pass


                    try:
                        preds = numpy.loadtxt(os.path.join(workfolder, g, item, 'predictions.txt'))
                        preds0 = preds[:, 0][preds[:, 1] == 0]
                        preds1 = preds[:, 0][preds[:, 1] == 1]

                        preds0 = proba_to_decision(preds0)
                        preds1 = proba_to_decision(preds1)

                        U, mwu_p_value = mannwhitneyu(preds0, preds1, alternative="two-sided")
                        data[g + ' ManWhitU'].append(mwu_p_value)
                        full_data.append([get_id(item), g, paradigm[0], 'ManWhitU', mwu_p_value, get_date(item), get_tura(item), N_targets, N_nontargets])

                        do_hists(preds0, preds1, mwu_p_value, AUC, os.path.join(workfolder, g, item))
                    except ValueError:
                        data[g + ' ManWhitU'].append(1.0)
                        full_data.append([get_id(item), g, paradigm[0], 'ManWhitU', 1.0, get_date(item), get_tura(item), N_targets, N_nontargets])
                    except IOError:
                        pass

                    try:
                        P_line = data_p.splitlines()[11]
                        P = float(P_line.strip(', ').split(':')[1])
                        print('P value', P)
                        data[g+' P'].append(P)
                        full_data.append([get_id(item), g, paradigm[0], 'P_boot', P, get_date(item), get_tura(item), N_targets, N_nontargets])
                    except IndexError:
                        pass



        df = pd.DataFrame.from_dict(data, orient='index').transpose()
        print(df)
        df.to_csv('{}/results_table_{}.csv'.format(workfolder, paradigm[0]))
        df_full_partial = pd.DataFrame.from_records(full_data, columns=full_data_headers)
        try:
            df_full = pd.DataFrame.from_csv('{}/results_database.csv'.format(workfolder,))
            df_full = pd.concat((df_full, df_full_partial))
        except:
            df_full = df_full_partial
        df_full.to_csv('{}/results_database.csv'.format(workfolder,))

    for paradigm in PARADIGMS.items():
        get_groups_for_paradigm(paradigm)


if __name__ == '__main__':
    try:
        workfolder = sys.argv[1]
        print "Workfolder:", workfolder
    except IndexError:
        workfolder = '.'
    get_groups(workfolder)

    # import pylab as pb
    # pb.hist(N_TARGETS["UWS"], bins=numpy.arange(1,1000))
    #
    # pb.show()