#!/usr/bin/env python2
# coding: utf8
from classify_score import boot_strap_aucs

boot_strap_aucs(average=1, pipeline_type='downsampling',
                 output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_dane_obkrojone_uszy_WSZYSTKIE_USZY/wyniki_downsampling/p300/',
                window=[0.100, 0.8])