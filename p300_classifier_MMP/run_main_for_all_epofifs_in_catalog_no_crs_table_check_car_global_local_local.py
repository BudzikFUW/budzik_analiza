#!/usr/bin/python
# coding: utf8

# ten skrypt analizuje pliki -epo.fif znalezione w poszczególnych podkatalogach podanej ścieżki (lub ścieżek)
# wyszukuje wszystkie pliki -epo.fif i łączy na następujące sposoby:
#   1. wszystkie, które są w tym samym katalogu i mają ten sam montaż (odpowiednik dawnego trybu 'both')
#   2. te, które zawierają ten sam typ bloku i ten sam montaż
#   3. TODO - dla global-local łączy bloki 1 z 2 oraz 3 z 4 jeśli mają ten sam montaż
import copy
import os
import sys
import time
import pandas as pd
from subprocess import Popen
from pprint import pprint


from collections import defaultdict

from miscellaneous import wait_for_a_process_to_end
Nproc = 8  # analyse_erp sam z siebie działa na wielu wątkach, więc nie ma sensu puszczać równolegle więcej niż dwóch
processes = []


INFO_FILE = '~/tabelka_CRS.csv'
INFO_DATA = pd.read_csv(INFO_FILE)

MISSING_FILES = []

def get_good(filename):
    return INFO_DATA[INFO_DATA['name'] == filename].good.values[0]

command = sys.argv[1]  # skrypt analizujący

if len(sys.argv) == 1:
    print "Podaj comendę oraz scieżkę do katalogu zawierającego pliki *-epo.fif"
else:

    montages_to_do = ['ears', 'Fz', 'Cz', 'car']
    montages_to_do = ['car',]
    montages_to_do = ['ears',]

    def default_dict_list():
        return defaultdict(list)

    same_root_and_montage = defaultdict(default_dict_list)

    for montage_to_do in montages_to_do:
        for catalog in sys.argv[2:]:
            for root, dirs, files in os.walk(catalog):
                for filename in files:  # przeglądamy rekursywnie wszystkie pliki w katalogu podanym, jako pierwszy argument w linii komend

                    if "-epo.fif" in filename:  # szukając plików *-epo.fif
                        path = os.path.join(root, filename)  # ścieżka do pliku z czystymi odcinkami *-epo.fif

                        pos_mont = filename.index("mont-")
                        pos_btype = filename.index("btype-")
                        pos_h = filename.index("h-")

                        montage = filename[pos_mont + 5: pos_btype - 1]
                        btype = filename[pos_btype + 6: pos_h - 1]

                        if montage_to_do in montage and btype=='local':

                            
                            same_root_and_montage[root][montage_to_do].append(path)
                            print filename.split('.')[0]
                            #~ print 'STATUS: ', bool(get_good(filename.split('.')[0]))
                            print montage

    montages_done_for_root = defaultdict(list)

    for montage_to_do in montages_to_do:

        if len(MISSING_FILES)>0:
            print "BRAKUJE PLIKÓW W TABELCE CRS:"
            for i in set(MISSING_FILES):
                print i
            sys.exit(1)
        for root in same_root_and_montage:
            filepaths = same_root_and_montage[root][montage_to_do]
            files_per_montage = len(filepaths)
            files_per_montages = [len(same_root_and_montage[root][i]) for i in montages_to_do]

            # dont do this montage, if not all files are present (all files == files in montage that preserves most files)
            if files_per_montage < max(files_per_montages):
                print "Not doing montage {} for {}\nNOT ALL FILES AVAILABLE IN THIS MONTAGE".format(montage_to_do, root)
                continue
            # dont do this montage if any previous was done
            if len(montages_done_for_root[root]) > 0:
                print "Not doing montage {} for {}\nANOTHER MONTAGE ALREADY DONE: {}".format(montage_to_do, root, montages_done_for_root[root])
                continue
            montages_done_for_root[root].append(montage_to_do)

            print "\nuruchamiam {} używając {} plików:".format(command, len(filepaths))
            pprint(filepaths)
            print
            try:
                processes.append(Popen((command,) + tuple(filepaths)))  # uruchomienie analizy osobno na każdym zniorze plików
                time.sleep(1)  # tak na wszelki wypadek, odczekam 1 sekundę po odpaleniu każdego skryptu
                # w razie gdyby miały sobie wejść w paradę przy tworzeniu katalogów, chociaż to chyba nie powinno się zdarzyć
            except OSError:
                pass

            # zabezpieczenie przed uruchomieniem zbyt wielu prcosów na raz i przeciążeniem w ten sposób maszyny
            if len(processes) >= Nproc:  # jeśli pracuje już Nproc procesów,
                wait_for_a_process_to_end(processes)

        # zaczekaj na zakończenie pozostałych procesów
        for p in processes:
            p.wait()
