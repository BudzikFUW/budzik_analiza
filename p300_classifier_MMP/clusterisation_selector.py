import mne
import copy
from sklearn.base import TransformerMixin
import scipy.signal as ss
import numpy
import numpy as np

from helper_functions.helper_data.ourcap_neighb import get_our_connectivity_sparse


P300_CHANNELS = 'F3;Fz;F4;C3;Cz;C4;P3;Pz;P4;O1;Oz;O2;M1;T5;T3;F7;Fp1;Fpz;Fp2;F8;T4;T6'.split(';')
MINIMAL_CLUSTER_RANGE = 0.0157  # seconds
MINIMAL_CLUSTER_WINDOW = 0.150
MAXIMAL_CLUSTER_WINDOW = 1.0

class EEGClusterisationSelector(TransformerMixin, object):
    """
    Finds significantly different clusters of EEG data between event classes.

    Returns features:
        mean of clusters by channel
    If not significant clusters found, most significant will be selected.

    self.clusters are saved as list of mask arrays (n_times, n_channels)
    """

    def __init__(self, Fs=128.0,
                 bas=-0.1,
                 window=1.1,
                 chnames=[],
                 cluster_p_level = 0.05,
                 use_global_cache = False,
                 cluster_time_channel_mask=True):
        self.Fs = Fs
        self.bas = bas
        self.window = window
        self.chnames = chnames
        self.clusters = None
        self.cluster_p_level = cluster_p_level
        self.p_level_achieved = None
        self.use_global_cache = use_global_cache
        self.GLOBAL_CHACHE = None
        self.fit_succesfull = False
        self.cluster_time_channel_mask=cluster_time_channel_mask

    def __repr__(self):
        return "<SpatioTemporalClusterisationSelector>"

    def create_P300_channel_mask(self, chnames, ntimes):
        mask = np.zeros((len(chnames), ntimes)).astype(bool)
        for i in xrange(len(chnames)):
            if chnames[i] in P300_CHANNELS:
                mask[i,:] = True
        self.P300_channel_mask = mask.T

        time_mask = np.zeros((len(chnames), ntimes)).astype(bool)

        minw = int((MINIMAL_CLUSTER_WINDOW - self.bas) * self.Fs)
        maxw = int((MAXIMAL_CLUSTER_WINDOW - self.bas) * self.Fs)

        time_mask[:, minw:maxw] = True
        self.P300_channel_mask *= time_mask.T

    def fit_smarttags(self, taglist):

        print 'LEN TAGLIST =', len(taglist), "|", [len(t) for t in taglist]
        min_length = min([min(i.get_samples().shape[1] for i in tags) for tags in taglist])

        data_tags = []
        for tags in taglist:
            data_tag = []
            for tag in tags:
                chnls_data = tag.get_channels_samples(self.chnames)[:, :min_length]
                if self.bas:
                    corrected_data = chnls_data - np.mean(chnls_data[:, 0:int(-self.Fs * self.bas)])  # baseline correction
                else:
                    corrected_data = chnls_data  # no baseline correction
                data_tag.append(corrected_data.T)
            data_tag = np.array(data_tag)
            data_tags.append(data_tag)

        features_l = []
        classes_l = []

        for cl, condition in enumerate(data_tags):
            features_per_cond = np.swapaxes(condition, 1, 2)
            features_l.append(features_per_cond)
            classes_l.append(np.ones(len(features_per_cond)) * cl)

        features = np.concatenate(features_l, axis=0)
        classes = np.concatenate(classes_l, axis=0)

        return self.fit(features, classes)

    def fit(self, features, classes):
        """Features: numpy array (n_observations x channels x times) or mne.Epochs."""
        # import IPython
        # IPython.embed()
        if isinstance(features, numpy.ndarray):
            chnames = self.chnames
        else:
            classes = features.events[:, 2]
            chnames = features.ch_names
            features = features.get_data()

        assert len(features.shape) == 3
        assert features.shape[1] == len(chnames)
        print "Fitting clusters for {} examples".format(len(features))

        self.create_P300_channel_mask(chnames, features.shape[2])

        if self.use_global_cache and self.GLOBAL_CHACHE is not None:
            self.clusters = self.GLOBAL_CHACHE.clusters
            self.p_level_achieved = self.GLOBAL_CHACHE.p_level_achieved
            self.fit_succesfull = self.GLOBAL_CHACHE.fit_succesfull
            return self

        classes_u = np.unique(classes)

        list_to_clusterize = []

        for cls in classes_u:
            features_cls = np.swapaxes(features[classes==cls], 1, 2)
            list_to_clusterize.append(features_cls)

        connectivity = get_our_connectivity_sparse(self.chnames)
        T_obs, clusters_, cluster_p_values, H0 = mne.stats.spatio_temporal_cluster_test(list_to_clusterize, step_down_p=0.05,
                                                                                        n_jobs=8, seed=42,
                                                                                        n_permutations=1024,
                                                                                        connectivity=connectivity,
                                                                                        out_type='mask')
        self.to_pickle = T_obs, clusters_, cluster_p_values, H0

        if self.cluster_time_channel_mask:
            masked_clusters = []
            masked_clusters_p_values = []
            for cluster, p_value in zip(clusters_, cluster_p_values):
                anything_left = self._filter_cluster_by_cluster_time_channel_mask(cluster)
                if anything_left:
                    masked_clusters.append(cluster)
                    masked_clusters_p_values.append(p_value)
            clusters_ = masked_clusters
            cluster_p_values = masked_clusters_p_values

        if len(clusters_) == 0:
            self.fit_succesfull = False
            return self
        else:
            self.fit_succesfull = True



        filtered_clusters, cluster_p_values_filtered = self._filter_clusters(clusters_, cluster_p_values)
        print "Found significant clusters (p level <= {}): {}".format(self.cluster_p_level, len(filtered_clusters))
        self.p_level_achieved = len(filtered_clusters) > 0
        if not self.p_level_achieved:
            filtered_clusters, cluster_p_values_filtered = self._select_most_significant_cluster(clusters_, cluster_p_values, )
            print "Using most significant cluster: p_level ", min(cluster_p_values)

        self.clusters = filtered_clusters
        self.cluster_p_values = cluster_p_values_filtered

        if self.use_global_cache:
            self.GLOBAL_CHACHE = self

        return self

    def _filter_cluster_by_cluster_time_channel_mask(self, cluster):
        """Clusters - list of arrays, changed in place"""

        cluster *= self.P300_channel_mask

        for chnn in xrange(len(self.chnames)):
            cluster_per_channel = cluster[:, chnn]
            ranges = self._find_cluster_ranges(cluster_per_channel)
            for range in ranges:
                ra_len = float(range[1] - range[0]) / self.Fs
                print "ra_len", ra_len, MINIMAL_CLUSTER_RANGE
                if abs(ra_len) < MINIMAL_CLUSTER_RANGE:
                    print "Removing cluster range:", range, cluster.shape
                    cluster[range[0]:range[1], chnn] = False

        return cluster.any()






    def transform(self, features):
        """Features: numpy array (n_observations x channels x times) or mne.Epochs"""

        if not isinstance(features, numpy.ndarray):
            features = features.get_data()


        observations = []
        for observation in features:
            observations.append(self._transform_obs(observation))
        # import IPython
        # IPython.embed()
        return numpy.array(observations)

    def _transform_obs(self, observation):
        """observation: numpy array (channels x times)."""
        features = []


        for cluster in self.clusters:

            if self.cluster_time_channel_mask:
                cluster_corr = cluster * self.cluster_time_channel_mask
            else:
                cluster_corr = cluster

            for channel in range(len(self.chnames)):
                import warnings
                warnings.filterwarnings('error')
                try:
                    cluster_per_chnl = cluster_corr[:, channel]
                    for ra in self._find_cluster_ranges(cluster_per_chnl):
                        mean = numpy.mean(observation[channel, ra[0]:ra[1]])
                        features.append(mean)
                except RuntimeWarning:
                    import IPython
                    IPython.embed()

        return features

    def _find_cluster_ranges(self, cluster):
        cluster_aug = numpy.concatenate([[False], cluster, [False]])  # if cluster is at start or end diff will not show anything
        cluster_start_stops = np.diff(cluster_aug.astype(int))
        cluster_starts = cluster_start_stops > 0
        cluster_starts = np.squeeze(np.argwhere(cluster_starts), axis=1)   # due to augmentation

        cluster_stops = cluster_start_stops < 0
        cluster_stops = np.squeeze(np.argwhere(cluster_stops), axis=1)   # due to augmentation


        ranges = []
        for start, stop in zip(cluster_starts, cluster_stops):
            ranges.append([start, stop])
        return ranges

    def find_cluster_ranges(self, cluster):
        "Find cluster ranges for cluster_per_channel."
        ranges_ind = self._find_cluster_ranges(cluster)
        ranges = [[i[0]/self.Fs + self.bas, i[1]/self.Fs + self.bas] for i in ranges_ind]
        return ranges

    def _select_most_significant_cluster(self, clusters_, cluster_p_values, ):
        return [clusters_[np.array(cluster_p_values).argmin()], ], [np.min(cluster_p_values), ]

    def _filter_clusters(self, clusters_, cluster_p_values):
        clusters_filtered = []
        cluster_p_values_filtered = []
        for cluster, p_value in zip(clusters_, cluster_p_values):
            if p_value <= self.cluster_p_level:
                clusters_filtered.append(cluster)
                cluster_p_values_filtered.append(p_value)
        return clusters_filtered, cluster_p_values_filtered


def _feature_extraction_singular(epoch, Fs, bas=-0.1,
                                 window=0.5,
                                 targetFs=30, ):
    '''performs feature extraction on epoch (array channels x time),
    Args:
        Fs: sampling in Hz
        bas: baseline in seconds
        targetFs: target sampling in Hz (will be approximated)
        window: timewindow after baseline to select in seconds

    Returns: 1D array downsampled, len = downsampled samples x channels
    epoch minus mean of baseline, downsampled by factor int(Fs/targetFs)
    samples used - from end of baseline to window timepoint
     '''
    #print "EPOCH", epoch.shape
    mean = np.mean(epoch[:, :-bas * Fs], axis=1)
    decimation_factor = int(1.0 * Fs / targetFs)
    selected = epoch[:, -bas * Fs:(-bas + window) * Fs] - mean[:, None]
    features = ss.decimate(selected, decimation_factor, axis=1, ftype='fir')
    return features.flatten()


