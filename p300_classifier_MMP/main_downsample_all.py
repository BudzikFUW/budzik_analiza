#!/usr/bin/env python2
# coding: utf8
from classify_score import boot_strap_aucs

boot_strap_aucs(average=1, pipeline_type='downsampling',
                 output_prefix='/repo/coma/DANE_SPORZADKOWANE/ANALIZA_P300_AUC/p300/',
                window=[0.200, 0.900], channels_to_use='F3,Fz,F4,C3,Cz,C4,P3,Pz,P4'.split(','))
