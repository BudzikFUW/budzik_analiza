#!/usr/bin/python2
# coding: utf-8
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib as mpl

N = 1000
n = 60

p = 0.05
data = np.loadtxt("mww-U-p{}-n=m-uppertail.dat".format(p))
U_crit = data[n-1]


p = 0.025
data = np.loadtxt("mww-U-p{}-n=m-uppertail.dat".format(p))
U_crit_2 = data[n-1]

print "\nspodziewana różnica dla przypadków niepoprawnych", U_crit/n**2, U_crit_2/n**2, U_crit_2/U_crit


###########################
# przypadek niby-poprawny #
###########################

def bimodal_on_interval(a = 0, b = 1, m1 = 0, m2 = 1, s = 0.2, size = 1):
    
    x = np.linspace(a, b, 1001)
    
    y = st.norm.pdf(x, loc = m1, scale = s) + st.norm.pdf(x, loc = m2, scale = s)
    y /= y.sum()*(x[1]-x[0])

    Y = np.zeros(1002)
    Y[1:] = np.cumsum(y)
    Y /= Y[-1]
    
    r = st.uniform.rvs(size = size)

    X = np.linspace(a, b, 1002)

    R = np.interp(r, Y, X)
    
    # # debug
    # plt.subplot(211)
    # plt.plot(X, Y)
    # plt.subplot(212)
    # plt.plot(x, y)
    # plt.show()
    return R


# R = bimodal_on_interval(size = 100000)


U_list = []

for i in range(N):
    a = bimodal_on_interval(size = n, m1 = -0.2)
    b = bimodal_on_interval(size = n, m1 = -0.2)
    U, p = st.mannwhitneyu(a, b, alternative = "greater")
    
    U_list.append(U)

print "std", np.std(U_list)/n**2, np.std(U_list)/len(U_list)**0.5/n**2

U_crit_sim = st.scoreatpercentile(U_list, 95)
print "bimodal 0-1, p = 0.05", U_crit/n**2, U_crit_sim/n**2, U_crit_sim / U_crit, np.mean(U_list)/n**2

# U_crit_sim_2 = st.scoreatpercentile(U_list, 97.5)
# print "odwrócony i przycięty Gauss, p = 0.025", U_crit_2, U_crit_sim_2, U_crit_sim_2 / U_crit_2
# print "odwrócony i przycięty Gauss", U_crit_2 / U_crit, U_crit_sim_2 / U_crit_sim
# print "odwrócony i przycięty Gauss", U_crit_sim / U_crit_2

exit()




###########################
# przypadki poprawne:  ####
###########################
print "\n\tprzypadki poprawne"

# # rokład normalny
# U_list = []
# for i in range(N):
#     a = st.norm.rvs(loc = 0, scale = 1, size = n)
#     b = st.norm.rvs(loc = 0, scale = 1, size = n)
#
#     U, p = st.mannwhitneyu(a, b, alternative = "greater")
#     U_list.append(U)
#
# U_crit_sim = st.scoreatpercentile(U_list, 95)
#
# print "rozklad normalny", U_crit, U_crit_sim, U_crit_sim/U_crit
#
#
#
# # rokład jednostajny
# U_list = []
# for i in range(N):
#     a = st.uniform.rvs(loc = 0, scale = 1, size = n)
#     b = st.uniform.rvs(loc = 0, scale = 1, size = n)
#
#     U, p = st.mannwhitneyu(a, b, alternative = "greater")
#     U_list.append(U)
#
# U_crit_sim = st.scoreatpercentile(U_list, 95)
#
# print "rozklad jednostajny", U_crit, U_crit_sim, U_crit_sim/U_crit
#
#
#
#
# # rokład dyskretny - poisson
# U_list = []
# for i in range(N):
#     a = st.poisson.rvs(n, size = n)
#     b = st.poisson.rvs(n, size = n)
#
#     U, p = st.mannwhitneyu(a, b, alternative = "greater")
#     U_list.append(U)
#
# U_crit_sim = st.scoreatpercentile(U_list, 95)
#
# print "rozklad poissona", U_crit, U_crit_sim, U_crit_sim/U_crit


###########################
# przypadki niepoprawne:  #
###########################
print "\n\tprzypadki niepoprawne"

# rokład normalny
U_list = []
for i in range(N):
    a = st.norm.rvs(loc = 0, scale = 1, size = n)
    b = st.norm.rvs(loc = 0, scale = 8, size = n)
    
    U, p = st.mannwhitneyu(a, b, alternative = "greater")
    U_list.append(U)

U_crit_sim = st.scoreatpercentile(U_list, 95)

print "rozklad normalny", U_crit, U_crit_sim, U_crit_sim / U_crit


# rokład jednostajny
U_list = []
for i in range(N):
    a = st.uniform.rvs(loc = 0, scale = 1, size = n)
    b = st.uniform.rvs(loc = -5, scale = 11, size = n)
    U, p = st.mannwhitneyu(a, b, alternative = "greater")
    U_list.append(U)

U_crit_sim = st.scoreatpercentile(U_list, 95)

print "rozklad jednostajny", U_crit, U_crit_sim, U_crit_sim / U_crit

# rokład dyskretny - poisson
U_list = []
for i in range(N):
    a = st.poisson.rvs(n, size = n)
    b = st.poisson.rvs(n*8, size = n) - 7*n
    
    U, p = st.mannwhitneyu(a, b, alternative = "greater")
    U_list.append(U)

U_crit_sim = st.scoreatpercentile(U_list, 95)

print "rozklad poissona", U_crit, U_crit_sim, U_crit_sim / U_crit


# odwrócony Gauss
U_list = []
for i in range(N):
    a = st.poisson.rvs(n, size = n)
    b = st.poisson.rvs(n * 8, size = n) - 7 * n
    
    U, p = st.mannwhitneyu(a, b, alternative = "greater")
    U_list.append(U)

U_crit_sim = st.scoreatpercentile(U_list, 95)

print "odwrócony Gauss", U_crit, U_crit_sim, U_crit_sim / U_crit