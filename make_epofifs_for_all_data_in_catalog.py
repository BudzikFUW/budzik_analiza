#!/usr/bin/python
# coding: utf8

import os
import sys
import time
from subprocess import Popen
from miscellaneous import wait_for_a_process_to_end
Nproc = 20
processes = []

if len(sys.argv) == 1:
    print "Podaj scieżkę do katalogu zawierającego pliki *.obci.raw"
else:

    for catalog in sys.argv[1:]:
        for root, dirs, files in os.walk(catalog):
            for filename in files:  # przeglądamy rekursywnie wszystkie pliki w katalogu podanym, jako pierwszy argument w linii komend
                if "obci.raw" in filename and ".etr." not in filename:  # szukając plików *.obci.raw, ale nie *.etr.obci.raw
                    path = os.path.join(root, filename)  # ścieżka do pliku danych *.obci.raw
                    
                    # zakładam, że trzymamy się przyjętej konwencji i nazwy katalogów są takie, jak prefiksy w nazwach skryptów
                    # a hierarchia jest taka, że katalog paradygmatu jest nadrzędny względem katalogu pacjenta
                    paradigm_catalog_name = os.path.basename(os.path.dirname(os.path.abspath(root))).replace("-", "_")
                    patient_catalog_name = os.path.basename(os.path.abspath(root))
                    
                    # #  dane tylko dla wybranej grupy pacjentów, a resztę pomijamy:
                    # if patient_catalog_name not in ("MW", ):
                    #     continue

                    # #  dane tylko dla wybranej grupy paradygmatów, a resztę pomijamy:
                    # if paradigm_catalog_name not in ("erds", ):
                    #     continue

                    #  dane tylko dla wybranej grupy paradygmatów, a resztę pomijamy:
                    # if not (("p300" in paradigm_catalog_name and "multim" not in paradigm_catalog_name) or "global" in paradigm_catalog_name):
                    #     continue

                    # #  dane tylko dla wybranej grupy paradygmatów, a resztę pomijamy:
                    # if paradigm_catalog_name not in ("erds", ):
                    #     continue

                    #  wykluczenie wybranej grupy paradygmatów
                    if paradigm_catalog_name in ("erds", "vep", "film") or "multi" in paradigm_catalog_name:
                        continue


                    # dzięki temu po nazwie katalogu nadrzędnego możemy ustalić nazwę skryptu do wykonania
                    if paradigm_catalog_name == "film":
                        paradigm_script = "p300_wzrokowe_animacja"
                    else:
                        paradigm_script = paradigm_catalog_name
                    command = "./" + paradigm_script + "_make_epofifs.py"
                    print "uruchamiam:", command, path
                    try:  # uruchomienie, jako osobnego procesu skryptu właściwego dla pliku, do którego prowadzi ścieżka path
                        processes.append(Popen((command, path)))
                        time.sleep(1)  # tak na wszelki wypadek, odczekam 1 sekundę po odpaleniu każdego skryptu
                        # w razie gdyby miały sobie wejść w paradę przy tworzeniu katalogów, chociaż to chyba nie powinno się zdarzyć
                    except OSError:
                        print "Nie można uruchomić polecenia:  {} {}\nPrzechodzę dalej.".format(command, path)
                    
                    # zabezpieczenie przed uruchomieniem zbyt wielu prcosów na raz i przeciążeniem w ten sposób maszyny
                    if len(processes) >= Nproc:  # jeśli pracuje już Nproc procesów,
                        if len(processes) >= Nproc:  # jeśli pracuje już Nproc procesów,
                            wait_for_a_process_to_end(processes)

# zaczekaj na zakończenie pozostałych procesów
for p in processes:
    p.wait()
os.remove("outdir_path.temp")
