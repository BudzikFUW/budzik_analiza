import pylab as pb
import glob
import datetime
import json
import sys
import os
import pandas
import seaborn
import numpy as np

from unified_data_metaanalisys.help_functions import merge_measurements_records
from eeg_artifacts.obci.analysis.obci_signal_processing.read_manager import ReadManager
from matplotlib.lines import Line2D
import tqdm

from collections import defaultdict

from pyexpat import ExpatError

from get_actigraph_date import get_actigraph_date

from crit_auc import auc_percentile
from utils import get_diagnosis, get_diagnosis_change_intervals_for_interval
from config import ordered_data_root
from merging_events import merge_measurements
import missing_fif_file_annotations


UNMERGED_DATABASE_COLUMNS = ['person_id', 'diag', 'measurement_type', 'measurement_date', 'file_path', 'fif_file_path']


class RecordingNameError(Exception):
    pass


def print_name_equivalence(name_eq_dict):
    to_print = defaultdict(list)
    l = sorted(name_eq_dict.items(), key=lambda x: x[1])
    for i in l:
        to_print[i[1]].append(i[0])
    i = 0
    for key, item in sorted(to_print.items()):
        i += 1
        print("{} ID: {}, names: {}".format(i, key, ', '.join(item)))


def read_names_equivalence():
    with open(ordered_data_root + 'equivalent_names.json', 'r') as dumpfile:
        name_equivalence_dict = json.load(dumpfile)
    return name_equivalence_dict


def organise_names_equivalense_and_check_crs_dzienne_base(dir):
    """Pierwsza funkcja dla odpalenia dla organizacji danych, używa dzienne dla stworzenia podstawowego słownika,
    ekwiwalencji i sprawdzenia czy wszyscy dzienni mają diagnozę CRS."""
    name_equivalence_dict = {}
    ###########################################
    name_equivalence_dict['jagoda'] = 'JSt'
    name_equivalence_dict['JAGODA'] = 'JSt'
    ###########################################

    for dirpath, dirnames, filenames in tqdm.tqdm(os.walk(dir, )):
        for filename in filenames:
            if filename[-4:] == '.raw' and 'etr' not in filename:
                name_folder = dirpath.split(os.path.sep)[-1]
                name_file = filename.split('_')[0].split('-')[0].split('.')[0]
                if not name_file in name_equivalence_dict.keys():
                    name_equivalence_dict[name_file] = name_folder

    all_ids = set(name_equivalence_dict.values())

    print_name_equivalence(name_equivalence_dict)
    with open(ordered_data_root + 'equivalent_names.json', 'w') as dumpfile:
        json.dump(name_equivalence_dict, dumpfile, indent=2, sort_keys=True)

    for i in all_ids:
        name = name_equivalence_dict.get(i, i)
        get_diagnosis(name, 2018, 1, 1)

def decipher_date(filename_without_name_and_extension):
    # podejścia do dat
    date_candidate_zimbabwe = filename_without_name_and_extension.split('-')[-1].split('_')[-1]
    date_candidate_month_name = filename_without_name_and_extension[-16:]

    try:
        date = datetime.datetime.strptime(date_candidate_month_name, '%Y_%b_%d_%H%M')
        return date
    except ValueError:
        pass

    try:
        date_candidate_film = filename_without_name_and_extension[-10:]
        date = datetime.datetime.strptime(date_candidate_film, '%d_%m_%Y')
        return date
    except ValueError:
        pass

    try:
        date_candidate_rs = filename_without_name_and_extension[-10:]
        date = datetime.datetime.strptime(date_candidate_rs[3:], '%d_%m_%y')
        return date
    except ValueError:
        pass

    try:
        date_candidate_rs_patients = filename_without_name_and_extension[-10:]
        date = datetime.datetime.strptime(date_candidate_rs_patients, '%d_%m_%Y')
        return date
    except ValueError:
        pass

    try:
        date_candidate_rs_patients_kuba = filename_without_name_and_extension[-10:]
        date = datetime.datetime.strptime(date_candidate_rs_patients_kuba[2:], '%d_%m_%y')
        return date
    except ValueError:
        pass

    try:
        date_candidate_rs_patients_backwards = filename_without_name_and_extension[-10:]
        date = datetime.datetime.strptime(date_candidate_rs_patients_backwards, '%Y-%m-%d')
        return date
    except ValueError:
        pass

    try:
        date_candidate_rs_weronika = filename_without_name_and_extension[-10:]
        date = datetime.datetime.strptime(date_candidate_rs_weronika, '%Y.%m.%d')
        return date
    except ValueError:
        pass

    if len(date_candidate_zimbabwe) == 15:
        date = datetime.datetime.strptime(date_candidate_zimbabwe, '%Y%m%dz%H%M%S')
    else:
        raise Exception("Unknown timeformat: {}".format(filename_without_name_and_extension))
    return date


def organise_dzienne_names(dir):
    name_equivalence_dict = read_names_equivalence()
    print(name_equivalence_dict)

    columns = UNMERGED_DATABASE_COLUMNS
    data = []
    for dirpath, dirnames, filenames in tqdm.tqdm(os.walk(dir, ), desc='organise dzienne'):
        for filename in filenames:
            filename_orig = filename[::]
            if filename[-4:] == '.raw' and 'etr' not in filename:

                name_file = filename.split('_')[0].split('-')[0].split('.')[0]
                name_normed = name_equivalence_dict.get(name_file, name_file)
                name_len = len(name_file)
                try:
                    if filename[-9:] == '.obci.raw':
                        filename = filename[:-9]
                    if filename[-4:] == '.raw':
                        filename = filename[:-4]
                except IndexError:
                    raise RecordingNameError(filename)
                filename_without_name = filename[name_len+1:]
                try:
                    date = decipher_date(filename_without_name)
                except Exception:
                    raise Exception('{}/{}\t{}'.format(dirpath, filename, filename_without_name))

                # typ pomiaru
                measurement_type = dirpath.split(os.path.sep)[-2]

                diagnosis = get_diagnosis(name_normed, int(date.year), int(date.month), int(date.day))
                # print(name_normed, pandas.Timestamp(date), measurement_type, name_normed, diagnosis, sep='\t\t')
                # print('name file: {}'.format(filename_without_name))
                # print(name_normed, filename, filename_without_name)
                data.append([name_normed, diagnosis, measurement_type, date, os.path.join(dirpath, filename_orig), []])
    frame_without_fifs = pandas.DataFrame.from_records(data, columns=columns)
    return frame_without_fifs


def visualise_measurement_dataframe(dataframe, only_for_patients=True, savefig=False):
    import pylab as pb
    import matplotlib.patches as mpatches

    if savefig:
        fig = pb.figure(figsize=(30, 20))
    else:
        fig = pb.figure()
    ax = fig.add_subplot(111)
    def type_name_key(name):
        if list(dataframe[dataframe.person_id==name].diag)[0] == 'CONTROL ADULT':
            pref = 0
        elif list(dataframe[dataframe.person_id==name].diag)[0] == 'CONTROL CHILD':
            pref = 1
        else:
            pref = 2
        key = str(pref) + name
        return key

    colors = {'p300-sluchowe-tony': 'blue',
              'p300-wzrokowe': 'green',
              'rs': 'black',
              'sep': 'yellow',
              'p300-czuciowe': 'brown',
              'PUSTE': 'white',
              'vep': 'lightgreen',
              'p300-sluchowe-slowa': 'darkblue',
              'erds': 'red',
              'p300-multimodal': 'orange',
              'film': 'gray',
              'global-local': 'lightblue',
              'nocne': 'violet',
              'actigraph': 'pink',}

    diag_colors = {'CONTROL ADULT': 'cadetblue',
                   'CONTROL CHILD': 'cadetblue',
                   'EMCS': 'teal',
                   'MCS+': 'darkturquoise',
                   'MCS+/EMCS': 'teal',
                   'MCS-': 'khaki',
                   'MCS-/UWS': 'beige',
                   'UWS': 'beige',
                   'UNKNOWN': 'white',
                   }

    patches = []
    for measurement_type in sorted(set(dataframe.measurement_type)):
        patch = Line2D([0], [0], marker='o', color='w', label=measurement_type,
               markerfacecolor=colors[measurement_type], markersize=8)
        patches.append(patch)

    for diag_type in ['UWS', 'MCS-/UWS', 'MCS-', 'MCS+', 'MCS+/EMCS', 'EMCS',
                      'CONTROL CHILD', 'CONTROL ADULT', 'UNKNOWN']:
        patch = mpatches.Patch(color=diag_colors[diag_type], label=diag_type)
        patches.append(patch)

    seperation = 10
    names = sorted(set(dataframe.person_id), key=type_name_key, reverse=True)
    name_numbers = np.arange(len(names)) * seperation

    ax.set_yticklabels(names)
    ax.set_yticks(name_numbers)

    min_date = min(dataframe.measurement_date).to_pydatetime()
    max_date = max(dataframe.measurement_date).to_pydatetime()

    for name_number, name in tqdm.tqdm(zip(name_numbers, names), desc='visualise'):
        if only_for_patients:
            if 'CONTROL' in get_diagnosis(name, 2018, 1, 1):
                continue
        for measurement_number, measurement_type in enumerate(sorted(set(dataframe.measurement_type))):
            subframe_person = dataframe[dataframe.person_id == name]
            subframe = subframe_person[subframe_person.measurement_type == measurement_type]
            x = [i.to_pydatetime() for i in list(subframe.measurement_date)]
            y = [name_number + measurement_number / len(colors.keys()) * seperation - 0.25*seperation] * len(x)

            diags, intervals = get_diagnosis_change_intervals_for_interval(name, min_date, max_date)

            import matplotlib.dates as mdates

            intervals_for_bar_plot = []

            for interval in intervals:
                start = mdates.date2num(interval[0])
                duration = mdates.date2num(interval[1])-mdates.date2num(interval[0])
                intervals_for_bar_plot.append([start, duration])

            diag_colors_bars = [diag_colors[i] for i in diags]
            ax.broken_barh(intervals_for_bar_plot, (name_number - 0.25*seperation, 0.5 *seperation),
                           facecolors=diag_colors_bars, zorder=5)#, alpha=0.3)

            ax.scatter(x, y, label=measurement_type, facecolor=colors[measurement_type], zorder=10)
    ax.legend(handles=patches)
    pb.grid()
    if savefig:
        fig.savefig(os.path.join(ordered_data_root, 'data_summary_all_{}.png'.format(not only_for_patients)))


def organise_actigraphs(path):
    columns = UNMERGED_DATABASE_COLUMNS
    records = []
    for dirpath, dirnames, filenames in tqdm.tqdm(os.walk(path), desc='organise actigraph'):
        for filename in filenames:
            filename_orig = filename[::]
            if filename[-4:] == '.bin':
                filename_without_ext = filename[:-4]
                person_id = filename_without_ext.split('_')[0]
                time_string = filename_without_ext.split('_')[-1]
                date = get_actigraph_date(os.path.join(dirpath, filename_orig))
                diagnosis = get_diagnosis(person_id, int(date.year), int(date.month), int(date.day))
                records.append([person_id, diagnosis, 'actigraph', date, os.path.join(dirpath, filename_orig), []])
    return pandas.DataFrame.from_records(records, columns=columns)


def organise_nocne(path):
    columns = UNMERGED_DATABASE_COLUMNS
    records = []
    for dirpath, dirnames, filenames in tqdm.tqdm(os.walk(path), desc='organise nocne'):
        for filename in filenames:
            filename_orig = filename[::]
            if filename[-4:] == '.raw':
                filename_without_ext = filename[:-4]
                person_id = filename_without_ext.split('_')[0]
                time_string = filename_without_ext.split('_', 1)[1][:10]
                date = datetime.datetime.strptime(time_string, '%d_%m_%Y')
                diagnosis = get_diagnosis(person_id, int(date.year), int(date.month), int(date.day))
                records.append([person_id, diagnosis, 'nocne', date, os.path.join(dirpath, filename_orig), []])
    return pandas.DataFrame.from_records(records, columns=columns)

def print_out_summary(merged_dataframe):
    columns = ['person_id', 'measurement_type', 'number_of_measurements', 'number_of_unique_crs']
    records = []
    frame = merged_dataframe.copy()
    for person in sorted(set(frame.person_id)):
        print("Person ID:", person)
        print('Measurement type'.rjust(20), '|' 'Number of measurements'.rjust(20),
              '|' 'Number of unique CRS'.rjust(20))
        for measurement_type in sorted(set(frame.measurement_type)):

            subframe_measurement_type = frame[frame.measurement_type == measurement_type]
            subframe = subframe_measurement_type[subframe_measurement_type.person_id == person]

            number_of_measurements = len(subframe)
            number_of_unique_crs = len(set(subframe.diag))
            if number_of_measurements > 0:
                print(str(measurement_type).rjust(20), '|',
                      str(number_of_measurements).rjust(20), '|', str(number_of_unique_crs).rjust(20))
            records.append([person, measurement_type, number_of_measurements, number_of_unique_crs])
    for group in ['CONTROL ADULT', 'CONTROL CHILD']:
        print("GROUP", group)
        subframe_group = frame[merged_dataframe.diag == group]
        for measurement_type in sorted(set(frame.measurement_type)):
            subframe_type = subframe_group[subframe_group.measurement_type == measurement_type]
            person_count = len(set(subframe_type.person_id))
            print('\t', measurement_type, person_count)

    print("GROUP PATIENTS")
    for measurement_type in sorted(set(frame.measurement_type)):
        mask = ~((frame.diag == 'CONTROL ADULT') | (frame.diag == 'CONTROL CHILD'))

        subframe_group = frame[mask]
        subframe_type = subframe_group[subframe_group.measurement_type == measurement_type]
        person_count = len(set(subframe_type.person_id))
        print('\t', measurement_type, person_count)

    dataframe = pandas.DataFrame.from_records(columns=columns, data=records)
    dataframe.to_csv(os.path.join(ordered_data_root, 'Measurements_dataframe_summary.csv'))


def list_files_without_artifacts_tags(dataframe):
    with open(os.path.join(ordered_data_root, 'missing_artifacts_list.txt'), 'w') as output:
        for path_to_raw in tqdm.tqdm(dataframe.file_path, desc='lst art'):
            if path_to_raw[-4:] == '.raw':
                core_of_path = path_to_raw[:-4]
                art_tagfile_path = core_of_path.rstrip(".obci")
                have_artifacts = False
                for i in glob.glob(art_tagfile_path+'*'):
                    if 'artifacts' in i:
                        have_artifacts = True
                        break

                if not have_artifacts:
                    output.write(path_to_raw)
                    output.write('\n')


class DataIntegrityException(Exception):
    pass


def check_data_integrity(dataframe):
    TAGLESS_MEASUREMENTS = ['nocne', 'rs']
    missing = []
    missing_tags = []
    corrupt_files = []
    wrong_channels = []
    for path_to_raw, measurement_type in tqdm.tqdm(zip(dataframe.file_path, dataframe.measurement_type),
                                                   desc='integrity',
                                                   total=len(dataframe.file_path)):
        if path_to_raw[-4:] == '.raw':
            core_of_path = path_to_raw[:-4]
            if not os.path.exists(core_of_path + '.xml'):
                missing.append(core_of_path + '.xml')
            if not os.path.exists(core_of_path + '.tag'):
                missing.append(core_of_path + '.tag')
            try:
                rm = ReadManager(core_of_path + '.xml', path_to_raw, core_of_path + '.tag')
                tags = rm.get_tags()
            except Exception:
                corrupt_files.append(path_to_raw)
                continue

            if 'ExG' in rm.get_param('channels_names')[0]:
                wrong_channels.append(path_to_raw)


            if measurement_type not in TAGLESS_MEASUREMENTS:
                if len(tags) == 0:
                    missing_tags.append(path_to_raw)

    if len(missing) > 0:
        print("MISSING FILES:")
        for i in missing:
            print(i)


    if len(missing_tags) > 0:
        print("MISSING TAGS:")
        for i in missing_tags:
            print(i)

    if len(corrupt_files) > 0:
        print("CORRUPT FILES:")
        for i in corrupt_files:
            print(i)

    if len(wrong_channels) > 0:
        print("WRONG CHANNEL NAMES:")
        for i in wrong_channels:
            print(i)

    if len(missing) or len(missing_tags) or len(corrupt_files):
        raise DataIntegrityException("CORRUPT DATABASE")

def check_fif_integrity(fif_folder, dataframe_without_fifs):
    dataframe = dataframe_without_fifs.copy(True)

    files_with_missing_fifs_count = 0
    with open(os.path.join(ordered_data_root, 'missing_fif_list.txt'), 'w') as output:

        for row_nr, row in tqdm.tqdm(dataframe.iterrows(), desc='check_fifs'):
            path_to_raw = row.file_path
            core_of_path = path_to_raw[:-4]
            raw_name_starts = os.path.basename(core_of_path.rstrip(".obci"))
            glob_path = os.path.join(fif_folder, row.measurement_type, row.person_id) + '/*/*/*.fif'
            fifs = []
            for fif in glob.glob(glob_path, recursive=True):
                if raw_name_starts in fif:
                    fifs.append(fif)

            if len(fifs) == 0:
                if not (row.measurement_type in ("erds", "vep", "film", "sep", 'rs') or "multi" in row.measurement_type):
                    if path_to_raw in missing_fif_file_annotations.annotations:
                        output.write('# ')
                    output.write(path_to_raw)
                    if path_to_raw in missing_fif_file_annotations.annotations:
                        output.write(' - ')
                        output.write(missing_fif_file_annotations.annotations[path_to_raw])
                    else:
                        files_with_missing_fifs_count += 1
                    output.write('\n')


            else:
                dataframe.ix[row_nr, 'fif_file_path'].extend(fifs)
    print("\n\n\nFILE COUNT WITH UNACCOUNTED MISSING FIFS:", files_with_missing_fifs_count, '\n\n\n')
    return dataframe

def get_auc_results_dzienne(dataframe):
    dataframe_original = dataframe.copy()
    dataframe_output = dataframe.copy()
    paradigms_folders = {'p300-sluchowe-tony': ['p300'],
                         'p300-wzrokowe': ['p300'],
                         'p300-czuciowe': ['p300'],
                         'p300-sluchowe-slowa': ['p300'],
                         'global-local': ['global_local_local_early', 'global_local_global_late']
                         }
    paradigms_features = {'p300-sluchowe-tony': ['auc_sluchowe_tony', 'auc_sluchowe_tony_perc'],
                         'p300-wzrokowe': ['auc_wzrokowe', 'auc_wzrokowe_perc'],
                         'p300-czuciowe': ['auc_czuciowe', 'auc_czuciowe_perc'],
                         'p300-sluchowe-slowa': ['auc_sluchowe_slowa', 'auc_sluchowe_slowa_perc'],
                         'global-local': ['auc_global_local_local_early',
                                          'auc_global_local_local_early_perc',
                                          'auc_global_local_global_late',
                                          'auc_global_local_global_late_perc']
                         }

    paradigm_executors = {'p300-sluchowe-tony': ['main_downsample_all.py'],
                         'p300-wzrokowe': ['main_downsample_wzrokowe.py'],
                         'p300-czuciowe': ['main_downsample_all.py'],
                         'p300-sluchowe-slowa': ['main_downsample_all.py'],
                         'global-local': ['main_downsample_global_local_global.py',
                                          'main_downsample_global_local_local.py']
                         }

    missing_aucs = []

    # import IPython
    # IPython.embed()
    for id, row in dataframe_original.iterrows():

        paradigm = row.measurement_type
        if paradigm not in paradigms_folders.keys():
            continue


        try:
            first_fif_base = os.path.basename(row.fif_file_paths[0]).split('.')[0]
        except IndexError:
            continue # brakuje fiffów

        try:
            # if paradigm == 'global-local':
            #     import IPython
            #     IPython.embed()
            for par_fol_id, paradigm_folder in enumerate(paradigms_folders[paradigm]):
                aucs_datafile_path = os.path.join(ordered_data_root, 'ANALIZA_P300_AUC', paradigm_folder,
                                                  'average_n_1_first_n_None', first_fif_base,
                                                  'data.json')

                with open(aucs_datafile_path) as aucs_datafile:
                    data = json.load(aucs_datafile)
                    if paradigms_features[paradigm][int(par_fol_id * 2)] not in dataframe_output.columns:
                        dataframe_output[paradigms_features[paradigm][par_fol_id * 2]] = None
                    if paradigms_features[paradigm][int(par_fol_id * 2 + 1)] not in dataframe_output.columns:
                        dataframe_output[paradigms_features[paradigm][par_fol_id * 2 + 1]] = None
                    dataframe_output.loc[id, paradigms_features[paradigm][int(par_fol_id * 2)]] = data['auc']
                    try:
                        dataframe_output.loc[id, paradigms_features[paradigm][int(par_fol_id * 2) + 1]] = get_auc_perc(data, paradigm_folder)
                    except KeyError:
                        print(row)
                        raise
        except FileNotFoundError:
            for executor in paradigm_executors[paradigm]:
                run_string = 'python2 ' + executor + " "
                for fiff in row.fif_file_paths:
                    run_string = run_string + '"' + fiff + '" '
                missing_aucs.append(run_string)

    with open(os.path.join(ordered_data_root, 'missing_auc_run_list_temp.txt'), 'w') as output:
        for i in missing_aucs:
            output.write(i)
            output.write('\n')

    return dataframe_output


def get_auc_perc(data, paradigm_folder):
    classes = data['class_counts']
    if paradigm_folder == 'global_local_global_late' or paradigm_folder == 'p300':
        classes_to_use = {'1': classes['target'],
                          '2': classes['nontarget'],
                          }
    elif paradigm_folder == 'global_local_local_early':
        try:
            classes_to_use = {'1': classes['dewiant'],
                              '2': classes['standard'],
                              }
        except KeyError:
            print(paradigm_folder)
            print(data)
            #raise
            classes_to_use = classes
    return auc_percentile(data['auc'], classes_to_use)


def get_erds_results(dataframe):
    dataframe_original = dataframe.copy()
    dataframe_output = dataframe.copy()

    erds_dataframe = pandas.DataFrame.from_csv(os.path.join(ordered_data_root, 'ANALIZA_ERD', 'erd-alpha-p-value.csv'),
                                               index_col=None)
    erds_features = set(erds_dataframe.columns) - {'file'}

    for feature in erds_features:
        if feature not in dataframe_output:
            dataframe_output[feature] = None
            dataframe_original[feature] = None

    missing_files = []
    for id, row in dataframe_original.iterrows():

        paradigm = row.measurement_type
        if paradigm != 'erds':
            continue

        for file_raw_nr, file_raw in enumerate(row.files_paths):
            file_base = os.path.basename(file_raw)
            row_modify = row.copy()

            for erds_feature in erds_features:
                feature_value = erds_dataframe[erds_dataframe.file == file_base][erds_feature]
                try:
                    row_modify[erds_feature] = feature_value.values[0]
                except IndexError:
                    pass
                    missing_files.append(file_raw)

            row_modify['files_paths'] = [file_raw, ]
            if file_raw_nr > 0:
                # chcemy rozdzielić jeśli jest kilka plików z pomiarami
                dataframe_output = dataframe_output.append(row_modify, ignore_index = True)
            else:
                dataframe_output.iloc[id] = row_modify

    if missing_files:
        print("\n\n\nFiles with missing ERDs analysis:")
        for i in missing_files:
            print(i)

    return dataframe_output


def get_nocne_results(dataframe):
    missing_files_sen = []
    missing_files_dtf = []
    dataframe_original = dataframe.copy()
    dataframe_output = dataframe.copy()

    nocne_sen_dataframe = pandas.DataFrame.from_csv(os.path.join(ordered_data_root, 'ANALIZA_SNOW', 'classification_parameters_s12_swa40.csv'),
                                               index_col=None)
    nocne_connectivity_dataframe = pandas.DataFrame.from_csv(os.path.join(ordered_data_root, 'ANALIZA_SNOW', 'ffdtf_vm_all_bands.csv'),
                                               index_col=None)
    nocne_sen_features = set(nocne_sen_dataframe.columns) - {'rec_id'}
    nocne_connectivity_features = set(nocne_connectivity_dataframe.columns) - {'rec_id'}

    for feature in list(nocne_sen_features) + list(nocne_connectivity_features):
        if feature not in dataframe_output:
            dataframe_output[feature] = None
            dataframe_original[feature] = None


    for id, row in dataframe_original.iterrows():
        paradigm = row.measurement_type
        if paradigm != 'nocne':
            continue

        for file_raw_nr, file_raw in enumerate(row.files_paths):
            file_base = os.path.basename(file_raw).split('.')[0]
            row_modify = row.copy()

            for feature in nocne_sen_features:
                feature_value = nocne_sen_dataframe[nocne_sen_dataframe.rec_id == file_base][feature]
                try:
                    row_modify[feature] = feature_value.values[0]
                except IndexError:
                    pass
                    missing_files_sen.append(file_raw)

            for feature in nocne_connectivity_features:
                feature_value = nocne_connectivity_dataframe[nocne_connectivity_dataframe.rec_id == file_base][feature]
                try:
                    row_modify[feature] = feature_value.values[0]
                except IndexError:
                    missing_files_dtf.append(file_raw)

            row_modify['files_paths'] = [file_raw, ]
            if file_raw_nr > 0:
                # chcemy rozdzielić jeśli jest kilka plików z pomiarami
                dataframe_output = dataframe_output.append(row_modify, ignore_index = True)
            else:
                dataframe_output.iloc[id] = row_modify

    missing_files_dtf = set(missing_files_dtf)
    missing_files_sen = set(missing_files_sen)

    if missing_files_sen:
        print("\n\n\nFiles with missing sleep analysis:")
        for i in sorted(missing_files_sen):
            print(i)

    if missing_files_dtf:
        print("\n\n\nFiles with missing dtf analysis:")
        for i in sorted(missing_files_dtf):
            print(i)

    return dataframe_output

def get_global_local_dtf(dataframe):
    missing_files_dtf = []
    dataframe_original = dataframe.copy()
    dataframe_output = dataframe.copy()


    connectivity_dataframe = pandas.DataFrame.from_csv(os.path.join(ordered_data_root, 'ANALIZA_GLOBAL_LOCAL_DTF', 'measures_vm_ffdtf.csv'),
                                               index_col=None)
    connectivity_dataframe = connectivity_dataframe.drop('skull', axis=1)
    not_renamed = ['name', 'diagnosis', 'measurement_date']

    connectivity_features_not_renamed = set(connectivity_dataframe.columns) - {'name', 'diagnosis', 'measurement_date'}
    connectivity_dataframe.rename(columns=lambda x: 'gl_' + x if x in connectivity_features_not_renamed else x, inplace=True)
    connectivity_features = set(connectivity_dataframe.columns) - {'name', 'diagnosis', 'measurement_date'}


    for feature in list(connectivity_features):
        if feature not in dataframe_output:
            dataframe_output[feature] = None
            dataframe_original[feature] = None

    for id, row in dataframe_original.iterrows():
        paradigm = row.measurement_type
        if paradigm != 'global-local':
            continue

        date = str(row.measurement_date)
        connectivity_row = connectivity_dataframe[connectivity_dataframe.measurement_date == date]
        if len(connectivity_row):
            if len(connectivity_row) > 1:
                raise Exception("Something is wrong with record {}".format(row))

            # there should be only 1 row ALWAYS
            row_dtf = list(connectivity_row.iterrows())[0][1]
            row_modify = row.copy()
            for feature in connectivity_features:
                row_modify[feature] = row_dtf[feature]
            dataframe_output.iloc[id] = row_modify

        else:
            missing_files_dtf.append(row.files_paths)

    if missing_files_dtf:
        print("\n\n\nGlobal Local files with missing dtf analysis:")
        for i in sorted(missing_files_dtf):
            print(i)
    return dataframe_output


def get_skulls(dataframe):
    skull_database = pandas.read_csv(os.path.join(ordered_data_root, 'CZASZKI.csv'))

    for id, row in skull_database.iterrows():
        p_id = row.patient_id
        mask = (dataframe.person_id == p_id)
        dataframe.loc[mask, 'skull'] = row.skull

    return dataframe


def generate_erp_list(dataframe):
    mask = np.array([len(i)>0 for i in dataframe.fif_file_paths])
    erp_draw_list = []
    for i in dataframe[mask].fif_file_paths:
        string = "python2 analyse_erp.py "
        for file in i:
            string += '"' + file + '" '
        erp_draw_list.append(string)

    with open(os.path.join(ordered_data_root, 'draw_erp_command_list.txt'), 'w') as output:
        for i in erp_draw_list:
            output.write(i)
            output.write('\n')

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print("Używanie: copy_and_rename.py katalog_z_nocnymi katalog_z_aktygrafami katalog_z_czystymi_fif_dla_dziennych katalog_z_dziennymi")
        sys.exit(1)
    organise_names_equivalense_and_check_crs_dzienne_base(sys.argv[-1])
    dataframe_dzienne_without_fifs = organise_dzienne_names(sys.argv[-1])
    dataframe_dzienne = check_fif_integrity(sys.argv[-2], dataframe_dzienne_without_fifs)
    dataframe_aktygraphy = organise_actigraphs(sys.argv[-3])
    dataframe_nocne = organise_nocne(sys.argv[-4])

    dataframe = pandas.concat([dataframe_dzienne, dataframe_nocne, dataframe_aktygraphy])

    try:
        check_data_integrity(dataframe)
    except DataIntegrityException:
        pass

    list_files_without_artifacts_tags(dataframe)

    dataframe.to_csv(os.path.join(ordered_data_root, 'Measurements_dataframe.csv'))
    # visualise_measurement_dataframe(dataframe)
    merged_dataframe = merge_measurements(dataframe)

    # check for auc results
    merged_dataframe = get_auc_results_dzienne(merged_dataframe)

    # check for erds results
    merged_dataframe = get_erds_results(merged_dataframe)

    merged_dataframe = get_nocne_results(merged_dataframe)

    #check for global_local_dtf
    merged_dataframe = get_global_local_dtf(merged_dataframe)

    #add skulls where needed

    merged_dataframe = get_skulls(merged_dataframe)


    merged_dataframe_csv = os.path.join(ordered_data_root, 'Measurements_database_merged.csv')
    merged_dataframe.sort_values(['person_id', 'measurement_date',]).to_csv()

    # merge_measurements
    merged_measurements_dataframe = merge_measurements_records(pandas.DataFrame.from_csv(merged_dataframe_csv))

    merged_measurements_dataframe.drop(['measurement_type',
                                        'files_paths',
                                        'fif_file_paths',
                                        ],
                                       axis=1).sort_values(['person_id', 'measurement_date',]).to_csv(
        os.path.join(ordered_data_root, 'Measurements_only_database_merged.csv')
    )

    generate_erp_list(merged_dataframe)

    visualise_measurement_dataframe(merged_dataframe, only_for_patients=True, savefig=True)
    visualise_measurement_dataframe(merged_dataframe, only_for_patients=False, savefig=True)
    pb.close('all')
    visualise_measurement_dataframe(merged_dataframe, only_for_patients=True, savefig=False)
    visualise_measurement_dataframe(merged_dataframe, only_for_patients=False, savefig=False)

    print_out_summary(merged_dataframe)

    pb.show()

