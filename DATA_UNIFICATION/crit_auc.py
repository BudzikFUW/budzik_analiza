from config import ordered_data_root
import os
import pandas
import scipy.stats as st
import json
_crit_file = open(os.path.join(os.path.dirname(__file__), 'ar_aucs.json'))
_crit_file = json.load(_crit_file)

def auc_percentile(auc, classes: dict):
    """classes - dict of class names and amount of epochs in that class
    auc - achieved auc
    return percentile of AR generated people
    """
    min_classes = min(classes.values())

    min_supported_classes = min([int(i) for i in _crit_file.keys()])
    max_supported_classes = max([int(i) for i in _crit_file.keys()])
    if min_classes < min_supported_classes:
        return None
    if min_classes > max_supported_classes:
        return auc_percentile(auc, {'1': max_supported_classes})

    percentile = st.percentileofscore(_crit_file[str(min_classes)], auc)

    return percentile
