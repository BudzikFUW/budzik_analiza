from config import ordered_data_root
import os
import pandas

dataframe = pandas.DataFrame.from_csv(os.path.join(ordered_data_root, 'Measurements_dataframe.csv'))

subframe = dataframe[dataframe.measurement_type == 'rs']
from eeg_artifacts.obci.analysis.obci_signal_processing import read_manager

for path_to_raw in subframe.file_path:
    core_of_path = path_to_raw.strip()[:-4]
    eeg_rm = read_manager.ReadManager(core_of_path + '.xml', core_of_path + '.raw', core_of_path + '.tag')
    channels_names = eeg_rm.get_param('channels_names')
    print(core_of_path + '.xml')
    print(channels_names)
