import csv
from datetime import datetime
import pandas

from config import ordered_data_root

crs_datafile = open(ordered_data_root + 'CRS_WYNIKI_OSTATECZNE.csv')
crs_data = list(csv.DictReader(crs_datafile))


class PatientNotFoundException(Exception):
    pass


def get_diagnosis_change_intervals_for_interval(name, start_datetime, end_datetime):
    if end_datetime < start_datetime:
        raise Exception('Wrong ordering of start and datetimes')
    f = pandas.DataFrame(crs_data)
    subject_frame = (f[f['inicjał'] == name]).copy()
    subject_frame['data'] = pandas.to_datetime(subject_frame['data'])
    sorted_frame = subject_frame.sort_values('data', ascending=True)

    meas_dates = []
    diags = []
    for measurement in sorted_frame.values:
         meas_dates.append(measurement[0].to_pydatetime())
         diags.append(measurement[1])

    min_date = min(meas_dates)
    max_date = max(meas_dates)

    if start_datetime > max_date:
        return [diags[-1]], [[start_datetime, end_datetime]]
    if end_datetime < min_date:
        return [diags[0]], [[start_datetime, end_datetime]]

    diags_out = []
    intervals = []
    if start_datetime < min_date:
        diags_out.append(diags[0])
        intervals.append([start_datetime, min_date])

    for i in range(len(meas_dates) - 1):
        if meas_dates[i] > end_datetime:
            break
        if start_datetime > meas_dates[i]:
            continue

        intervals.append([meas_dates[i], meas_dates[i+1]])
        diags_out.append(diags[i])

    if meas_dates[-1] < end_datetime:
        diags_out.append(diags[-1])
        intervals.append([meas_dates[-1], end_datetime])

    return diags_out, intervals

def get_diagnosis(name, year, month, day):
    """
    Returns latest diagnosis for given date
    If date precedes any given measurement returns first measurement.
    """
    rows = []
    for row in crs_data:
        if row['inicjał'].lower() == name.lower():
            try:
                timestamp_date = datetime.strptime(row['data'], '%Y-%m-%d').timestamp()
            except ValueError:
                raise ValueError(row['data'])
            rows.append([timestamp_date, row['diagnoza_ost']])
    if len(rows) == 0:
        raise PatientNotFoundException(name)

    rows = sorted(rows, reverse=True)
    diagnosis_time = datetime(year, month, day).timestamp()
    for row in rows:
        if diagnosis_time >= row[0]:
            return row[1]
    return rows[-1][1]
