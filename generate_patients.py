#!/usr/bin/python2
# coding: utf-8
import sys
import time
from scipy.signal import iirdesign

import pickle
import numpy as np

from pprint import pprint
import matplotlib.pyplot as plt

from obci.analysis.obci_signal_processing.read_manager import ReadManager

from helper_functions.helper_functions import PrepareRM
from helper_functions.mne_conversions import nparrays_to_mne
from patients_generator.ar_generators import fit_ar_to_data, generate_fake_eeg_using_ar, apply_filters, cut_eeg_by_tags, generate_fake_tags


try:
    from scipy.signal import iirnotch
    iirnotch_available = True
except ImportError:
    iirnotch_available = False


def get_microvolt_samples(stag, channels = None):
    """Does get_samples on smart tag (read manager), but multiplied by channel gain"""
    if not channels:  # returns for all channels
        channels = stag.get_param('channels_names')
    gains = stag.get_param('channels_gains')
    gains = np.array([float(gains[channels.index(ch)]) for ch in channels], ndmin = 2).T
    return stag.get_channels_samples(channels) * gains


montage = []
# kontrola MP wzrokowe ears i jego kanały:

# PATIENT_TO_USE = "/home/marcin/budzik_results/p300-wzrokowe/MP/tura_3/ica_and_artifacts/MP_P300_wz1_20170427z151346.obci['ears', 'M1', 'M2']-5836033011891004619_ICA_CLEANED.obci"
# # PATIENT_TO_USE = "/repo/fizmed/coma/results/mpietrzak/Sun-Nov-26-13:06:27-2017-(ears_CA_Fz_Cz_control-preanalysed)/p300-wzrokowe/MP/tura_3/ica_and_artifacts/MP_P300_wz1_20170427z151346.obci['ears', 'M1', 'M2']-5836033011891004619_ICA_CLEANED.obci"
#
# CHANNELS_TO_USE = ('Fp1', 'Fpz', 'Fp2',
#                    'F7', 'F3', 'Fz', 'F4', 'F8',
#                    'T3',
#                    'C3', 'Cz', 'C4',
#                    'T4',
#                    'T5',
#                    'P3', 'Pz', 'P4',
#                    'T6',
#                    'O1', 'Oz', 'O2')



# kontrola MP wzrokowe car i jego kanały:

# PATIENT_TO_USE = "/home/marcin/budzik_results/p300-wzrokowe/MP/tura_3/ica_and_artifacts/MP_P300_wz1_20170427z151346.obci['car']2960349644512197368_ICA_CLEANED.obci"
# PATIENT_TO_USE = "/repo/fizmed/coma/results/mpietrzak/Sun-Nov-26-13:06:27-2017-(ears_CA_Fz_Cz_control-preanalysed)/p300-wzrokowe/MP/tura_3/ica_and_artifacts/MP_P300_wz1_20170427z151346.obci['car']2960349644512197368_ICA_CLEANED.obci"
#
# CHANNELS_TO_USE = ('Fp1', 'Fpz', 'Fp2',
#                    'F7', 'F3', 'Fz', 'F4', 'F8',
#                    'T3',
#                    'C3', 'Cz', 'C4',
#                    'T4',
#                    'T5',
#                    'P3', 'Pz', 'P4',
#                    'T6',
#                    'O1', 'Oz', 'O2')



# pacjent WS wzrokowe ears i jego kanały:

# PATIENT_TO_USE = "/home/marcin/budzik_results/p300-wzrokowe/WS/tura_3/ica_and_artifacts/weronika_P300_wz1_20170501z114509.obci['ears', 'M1', 'M2']3366085856821639851_ICA_CLEANED.obci"
# PATIENT_TO_USE = "/repo/fizmed/coma/results/mpietrzak/Sun-Nov-26-15:23:26-2017-(ears_CA_Fz_Cz_patients-preanalysed)/p300-wzrokowe/WS/tura_3/ica_and_artifacts/weronika_P300_wz1_20170501z114509.obci['ears', 'M1', 'M2']3366085856821639851_ICA_CLEANED.obci"
#
# CHANNELS_TO_USE = ('Fp1', 'Fpz', 'Fp2',
#                    'F3', 'Fz', 'F8',
#                    'T3',
#                    'C3', 'Cz', 'C4',
#                    'T4',
#                    'T5',
#                    'P3', 'Pz', 'P4',
#                    'T6',
#                    'O1', 'Oz', 'O2')


# pacjent MW wzrokowe ears  i jego kanały:

# PATIENT_TO_USE = "/home/marcin/budzik_results/p300-wzrokowe/MW/tura_3/ica_and_artifacts/mateusz_P300_wz1_20170512z100837.obci['ears', 'M1', 'M2']-2238498526219703693_ICA_CLEANED.obci"
# PATIENT_TO_USE = "/repo/fizmed/coma/results/mpietrzak/Sun-Nov-26-15:23:26-2017-(ears_CA_Fz_Cz_patients-preanalysed)/p300-wzrokowe/MW/tura_3/ica_and_artifacts/mateusz_P300_wz1_20170512z100837.obci['ears', 'M1', 'M2']-2238498526219703693_ICA_CLEANED.obci"
#
# CHANNELS_TO_USE = ('Fp1', 'Fpz', 'Fp2',
#                    'F7', 'F3', 'Fz', 'F4', 'F8',
#                    'T3',
#                    'C3', 'Cz', 'C4',
#                    'T4',
#                    'T5',
#                    'P3', 'Pz', 'P4',
#                    'T6',
#                    'O1', 'Oz', 'O2')


# pacjent MW rs ears i jego kanały - bec ICA:
# ponieważ to surowe dane, to trzeba zmontować:
montage = ['ears', 'M1', 'M2']
# PATIENT_TO_USE = "/media/reszta/BUDZIKdane/2017-08/rs/MW/MW_rs_16_06_2017"
PATIENT_TO_USE = "/home/budzik/dane_pomiarowe/2017-08/rs/MW/MW_rs_16_06_2017"
#
CHANNELS_TO_USE = ('Fp1', 'Fpz', 'Fp2',
                   'F7', 'F3', 'Fz', 'F4',
                   'T3',
                   'C3', 'Cz', 'C4',
                   'T4',
                   'T5',
                   'P3', 'Pz', 'P4',
                   'T6',
                   'O1', 'Oz', 'O2')





targetsN = int(sys.argv[1])
nontargetsN = targetsN * 4
epoch_length = 1.1  # s

order = 21
number_of_patients = int(sys.argv[2])
first_patient_number = int(sys.argv[3])

rm = ReadManager(PATIENT_TO_USE+'.xml', PATIENT_TO_USE+'.raw', PATIENT_TO_USE+'.tag')
if montage:
    rm = PrepareRM(rm, montage = montage)


Fs = float(rm.get_param('sampling_frequency'))
Nyq = Fs/2
pprint(rm.get_params())

f_budzik = iirdesign(1/Nyq, 0.5/Nyq, 3, 6.97, ftype='butter')
f_budzik2 = iirdesign(30/Nyq, 60/Nyq, 3, 12.4, ftype='butter')
if iirnotch_available:
    f_lines = [iirnotch(i/Nyq, i/5.0) for i in [50, 100, 150, 200, 250, 300, 350, 400, 450, 500]]
    f_line = [iirnotch(i/Nyq, i/5.0) for i in [50]]
else:
    f_lines = pickle.loads("(lp0\n((lp1\ncnumpy.core.multiarray\nscalar\np2\n(cnumpy\ndtype\np3\n(S'f8'\np4\nI0\nI1\ntp5\nRp6\n(I3\nS'<'\np7\nNNNI-1\nI-1\nI0\ntp8\nbS'\\xf2T\\x06\\xab9\\x84\\xef?'\np9\ntp10\nRp11\nag2\n(g6\nS'\\xa1F\\xc3\\xe5|\\x0b\\xfe\\xbf'\np12\ntp13\nRp14\nag2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np15\ntp16\nRp17\na(lp18\ng2\n(g6\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?'\np19\ntp20\nRp21\nag2\n(g6\nS'\\xa1F\\xc3\\xe5|\\x0b\\xfe\\xbf'\np22\ntp23\nRp24\nag2\n(g6\nS'\\xe4\\xa9\\x0cVs\\x08\\xef?'\np25\ntp26\nRp27\natp28\na((lp29\ng2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np30\ntp31\nRp32\nag2\n(g6\nS')r\\xbf\\\\u\\xc4\\xf9\\xbf'\np33\ntp34\nRp35\nag2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np36\ntp37\nRp38\na(lp39\ng2\n(g6\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?'\np40\ntp41\nRp42\nag2\n(g6\nS')r\\xbf\\\\u\\xc4\\xf9\\xbf'\np43\ntp44\nRp45\nag2\n(g6\nS'\\xe4\\xa9\\x0cVs\\x08\\xef?'\np46\ntp47\nRp48\natp49\na((lp50\ng2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np51\ntp52\nRp53\nag2\n(g6\nS'\\x1c\\x0c\\x85Df\\x15\\xf3\\xbf'\np54\ntp55\nRp56\nag2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np57\ntp58\nRp59\na(lp60\ng2\n(g6\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?'\np61\ntp62\nRp63\nag2\n(g6\nS'\\x1c\\x0c\\x85Df\\x15\\xf3\\xbf'\np64\ntp65\nRp66\nag2\n(g6\nS'\\xe4\\xa9\\x0cVs\\x08\\xef?'\np67\ntp68\nRp69\natp70\na((lp71\ng2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np72\ntp73\nRp74\nag2\n(g6\nS'\\xe8\\xef)\\xda4<\\xe5\\xbf'\np75\ntp76\nRp77\nag2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np78\ntp79\nRp80\na(lp81\ng2\n(g6\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?'\np82\ntp83\nRp84\nag2\n(g6\nS'\\xe8\\xef)\\xda4<\\xe5\\xbf'\np85\ntp86\nRp87\nag2\n(g6\nS'\\xe4\\xa9\\x0cVs\\x08\\xef?'\np88\ntp89\nRp90\natp91\na((lp92\ng2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np93\ntp94\nRp95\nag2\n(g6\nS'M\\x89\\x99\\xe2\\x80\\x8f\\xb2\\xbf'\np96\ntp97\nRp98\nag2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np99\ntp100\nRp101\na(lp102\ng2\n(g6\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?'\np103\ntp104\nRp105\nag2\n(g6\nS'M\\x89\\x99\\xe2\\x80\\x8f\\xb2\\xbf'\np106\ntp107\nRp108\nag2\n(g6\nS'\\xe4\\xa9\\x0cVs\\x08\\xef?'\np109\ntp110\nRp111\natp112\na((lp113\ng2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np114\ntp115\nRp116\nag2\n(g6\nS'\\x8f\\xe1l\\x16\\xcc\\xcf\\xe0?'\np117\ntp118\nRp119\nag2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np120\ntp121\nRp122\na(lp123\ng2\n(g6\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?'\np124\ntp125\nRp126\nag2\n(g6\nS'\\x8f\\xe1l\\x16\\xcc\\xcf\\xe0?'\np127\ntp128\nRp129\nag2\n(g6\nS'\\xe4\\xa9\\x0cVs\\x08\\xef?'\np130\ntp131\nRp132\natp133\na((lp134\ng2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np135\ntp136\nRp137\nag2\n(g6\nS'\\x19O\\xa1\\n\\xce/\\xf1?'\np138\ntp139\nRp140\nag2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np141\ntp142\nRp143\na(lp144\ng2\n(g6\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?'\np145\ntp146\nRp147\nag2\n(g6\nS'\\x19O\\xa1\\n\\xce/\\xf1?'\np148\ntp149\nRp150\nag2\n(g6\nS'\\xe4\\xa9\\x0cVs\\x08\\xef?'\np151\ntp152\nRp153\natp154\na((lp155\ng2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np156\ntp157\nRp158\nag2\n(g6\nS'\\x83\\x17o\\x8d\\xd2\\\\\\xf8?'\np159\ntp160\nRp161\nag2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np162\ntp163\nRp164\na(lp165\ng2\n(g6\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?'\np166\ntp167\nRp168\nag2\n(g6\nS'\\x83\\x17o\\x8d\\xd2\\\\\\xf8?'\np169\ntp170\nRp171\nag2\n(g6\nS'\\xe4\\xa9\\x0cVs\\x08\\xef?'\np172\ntp173\nRp174\natp175\na((lp176\ng2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np177\ntp178\nRp179\nag2\n(g6\nS'l\\xed\\x8aneC\\xfd?'\np180\ntp181\nRp182\nag2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np183\ntp184\nRp185\na(lp186\ng2\n(g6\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?'\np187\ntp188\nRp189\nag2\n(g6\nS'l\\xed\\x8aneC\\xfd?'\np190\ntp191\nRp192\nag2\n(g6\nS'\\xe4\\xa9\\x0cVs\\x08\\xef?'\np193\ntp194\nRp195\natp196\na((lp197\ng2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np198\ntp199\nRp200\nag2\n(g6\nS'\\x172\\t3]n\\xff?'\np201\ntp202\nRp203\nag2\n(g6\nS'\\xf2T\\x06\\xab9\\x84\\xef?'\np204\ntp205\nRp206\na(lp207\ng2\n(g6\nS'\\x00\\x00\\x00\\x00\\x00\\x00\\xf0?'\np208\ntp209\nRp210\nag2\n(g6\nS'\\x172\\t3]n\\xff?'\np211\ntp212\nRp213\nag2\n(g6\nS'\\xe4\\xa9\\x0cVs\\x08\\xef?'\np214\ntp215\nRp216\natp217\na.")
    f_line = [iirdesign([47.5/Nyq, 52.5/Nyq], [49.9/Nyq, 50.1/Nyq], 3, 25, ftype="cheby2")]

filters = [f_budzik, ] + f_lines

data_org = get_microvolt_samples(rm, CHANNELS_TO_USE)[:, int(Fs * 15):-int(Fs * 10)]
data = apply_filters(data_org, filters)[:, int(2 * Fs):int(-2 * Fs)]
Av = fit_ar_to_data(data, order)

filters = [f_budzik, f_budzik2] + f_line
data = apply_filters(data_org, filters)
data_mean_abs = np.mean(np.abs(data))
for pat_no in range(first_patient_number, first_patient_number + number_of_patients):
    target_tags, nontarget_tags = generate_fake_tags(epoch_length, targetsN, nontargetsN, Fs)
    length = int(max(target_tags + nontarget_tags) + (epoch_length + 10) * Fs)
    print "\n", pat_no
    print "eeg_length", length
    
    eeg_org = generate_fake_eeg_using_ar(Av, length)

    eeg = apply_filters(eeg_org, filters)
    # eeg = eeg_org.copy()

    print "przed normalizacją amplitud"
    print 'sr', np.mean(np.abs(data)), np.mean(np.abs(eeg))
    print 'std', np.std(data), np.std(eeg)
    eeg *= data_mean_abs/np.mean(np.abs(eeg))  # normalizacja amplitud
    print "po normalizacji amplitud"
    print 'sr', np.mean(np.abs(data)), np.mean(np.abs(eeg))
    print 'std', np.std(data), np.std(eeg)
    
    target_epochs = cut_eeg_by_tags(eeg, target_tags, int(epoch_length * Fs)) * 1e-6
    nontarget_epochs = cut_eeg_by_tags(eeg, nontarget_tags, int(epoch_length * Fs)) * 1e-6
    
    nparrays_to_mne(target_epochs, nontarget_epochs,
                    "/repo/fizmed/coma/results/mpietrzak/simulated_on_MW_rs_ears/AR{}".format(targetsN),
                    'AR{}S{:03d}_P300_sim_{}'.format(targetsN, pat_no, time.strftime("%Y%m%dz%H%M%S")),
                    montage = ['ears', 'M1', 'M2'],
                    chnames = CHANNELS_TO_USE,
                    plot_only = False)
