#!/usr/bin/python
# coding: utf8

# ten skrypt analizuje pliki -epo.fif znalezione w poszczególnych podkatalogach podanej ścieżki (lub ścieżek)
# wyszukuje wszystkie pliki -epo.fif i łączy na następujące sposoby:
#   1. wszystkie, które są w tym samym katalogu i mają ten sam montaż (odpowiednik dawnego trybu 'both')
#   2. te, które zawierają ten sam typ bloku i ten sam montaż
#   3. TODO - dla global-local łączy bloki 1 z 2 oraz 3 z 4 jeśli mają ten sam montaż

import os
import sys
import time
from multiprocessing import cpu_count
from subprocess import Popen
from miscellaneous import wait_for_a_process_to_end
Nproc = cpu_count()/4  # analyse_erp sam z siebie działa na wielu wątkach, więc nie ma sensu puszczać równolegle więcej niż dwóch
processes = []

command = "./analyse_erp.py"  # skrypt analizujący

if len(sys.argv) == 1:
    print "Podaj scieżkę do katalogu zawierającego pliki *-epo.fif"
else:
    same_root_and_montage = {}
    same_root_montage_and_btype = {}
    already_done = set()

    for catalog in sys.argv[1:]:
        for root, dirs, files in os.walk(catalog):
            for filename in files:  # przeglądamy rekursywnie wszystkie pliki w katalogu podanym, jako pierwszy argument w linii komend
                if "-epo.fif" in filename:  # szukając plików *-epo.fif
                    path = os.path.join(root, filename)  # ścieżka do pliku z czystymi odcinkami *-epo.fif

                    pos_mont = filename.index("mont-")
                    pos_btype = filename.index("btype-")
                    pos_h = filename.index("h-")
                    
                    montage = filename[pos_mont + 5: pos_btype - 1]
                    btype = filename[pos_btype + 6: pos_h - 1]
                    
                    if btype not in ("local", "global"):
                        if (root, montage) not in same_root_and_montage:
                            same_root_and_montage[(root, montage)] = []
                        same_root_and_montage[(root, montage)].append(path)
                        
                    if (root, montage, btype) not in same_root_montage_and_btype:
                        same_root_montage_and_btype[(root, montage, btype)] = []
                    same_root_montage_and_btype[(root, montage, btype)].append(path)
                    
    for root_montage in same_root_and_montage:
        filepaths = tuple(same_root_and_montage[root_montage])
        if filepaths in already_done:
            continue
        already_done.add(filepaths)
           
        print "uruchamiam:", command, filepaths
        try:
            processes.append(Popen((command,) + filepaths))  # uruchomienie analizy osobno na każdym zniorze plików
            time.sleep(1)  # tak na wszelki wypadek, odczekam 1 sekundę po odpaleniu każdego skryptu
            # w razie gdyby miały sobie wejść w paradę przy tworzeniu katalogów, chociaż to chyba nie powinno się zdarzyć
        except OSError:
            pass
        
        # zabezpieczenie przed uruchomieniem zbyt wielu prcosów na raz i przeciążeniem w ten sposób maszyny
        if len(processes) >= Nproc:  # jeśli pracuje już Nproc procesów,
            wait_for_a_process_to_end(processes)

    for root_montage_btype in same_root_montage_and_btype:
        filepaths = tuple(same_root_montage_and_btype[root_montage_btype])
        if filepaths in already_done:
            continue
        already_done.add(filepaths)
    
        print "uruchamiam:", command, filepaths
        try:
            processes.append(Popen((command,) + filepaths))  # uruchomienie analizy osobno na każdym zniorze plików
            time.sleep(1)  # tak na wszelki wypadek, odczekam 1 sekundę po odpaleniu każdego skryptu
            # w razie gdyby miały sobie wejść w paradę przy tworzeniu katalogów, chociaż to chyba nie powinno się zdarzyć
        except OSError:
            pass

        # zabezpieczenie przed uruchomieniem zbyt wielu prcosów na raz i przeciążeniem w ten sposób maszyny
        if len(processes) >= Nproc:  # jeśli pracuje już Nproc procesów,
            wait_for_a_process_to_end(processes)
    
    # zaczekaj na zakończenie pozostałych procesów
    for p in processes:
        p.wait()
